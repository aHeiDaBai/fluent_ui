#include <string.h>

#include "my_ui.h"

static int8_t   temp    = -50;
static uint16_t meter   = 9802;
static float    speed   = -3.4789;
static float    voltage = 6.9;
static float    current = 2.1;
static float    acce    = 0.841;
static float    Kp = 1.2;
static float    Ki = 0.1;
static float    Kd = 0.7;
static float    presicion = 0.1;

/*第一页中展示的内容*/
ui_obj_t speed_item, acce_item, meter_item, cur_item, vol_item, tmp_item;
ui_obj_t pid_page, precision_page;

/*PID页面中展示的内容*/
static ui_obj_t PID_type_item, Kp_item, Ki_item, Kd_item, motor_item;

/*Presicion页面中展示的内容*/
static ui_obj_t precision1_item, precision2_item, precision3_item;

static char PID_type_buffer[10];

void ui_init(void)
{
	/*首先进行界面设计*/
	/*第一页中的内容（注意第三个参数均为&first_page）：*/
    ui_add_item(&speed_item, "- Speed",   &first_page, 0, UI_FLOAT_ITEM,  &speed,   3);
    ui_add_item(&acce_item,  "- Acce",    &first_page, 1, UI_FLOAT_ITEM,  &acce,    3);
    ui_add_page(&pid_page,   "+ PID",     &first_page, 2);
    ui_add_item(&meter_item, "- Meter",   &first_page, 3, UI_UINT16_ITEM, &meter,   0);
    ui_add_item(&cur_item,   "- Current", &first_page, 4, UI_FLOAT_ITEM,  &current, 2);
    ui_add_item(&vol_item,   "- Voltage", &first_page, 5, UI_FLOAT_ITEM,  &voltage, 1);
	ui_add_item(&tmp_item,   "- Temp",    &first_page, 6, UI_INT8_ITEM,   &temp,    0);
    ui_add_page(&precision_page, "+ Precision", &first_page, 7);
	
    /*PID页面中的内容（注意第三个参数均为&pid_page）：*/
	memset(PID_type_buffer, 0, sizeof(PID_type_buffer));
	strcpy(PID_type_buffer, "*PI*");
	ui_add_item(&PID_type_item, "- Type",  &pid_page, 0, UI_STRING,         PID_type_buffer, 0);
    ui_add_item(&Kp_item,       "# Kp",    &pid_page, 1, UI_FLOAT_ITEM,     &Kp,  4);
    ui_add_item(&Ki_item,       "# Ki",    &pid_page, 2, UI_FLOAT_ITEM,     &Ki,  4);
    ui_add_item(&Kd_item,       "# Kd",    &pid_page, 3, UI_FLOAT_ITEM,     &Kd,  4);
    ui_add_item(&motor_item,    "- Motor", &pid_page, 4, UI_CHECK_BOX_ITEM, NULL, 0);
	
	/*Precision页面中的内容*/
	ui_add_item(&precision1_item, "#preci1", &precision_page, 0, UI_FLOAT_ITEM, &presicion, 1);
	ui_add_item(&precision2_item, "#preci2", &precision_page, 1, UI_FLOAT_ITEM, &presicion, 2);
	ui_add_item(&precision3_item, "#preci3", &precision_page, 2, UI_FLOAT_ITEM, &presicion, 3);
	
	/*最后调用ui_settings_init函数*/
	/*注意：必须在界面全部设计完后才可以调用ui_settings_init函数*/
	ui_settings_init();
}

void ui_add(void)
{
	 ui_type_t cursor_type = ui_get_cursor_obj_type();
	 switch (cursor_type){
		 case UI_FLOAT_ITEM:
			 ui_inc_cursor_obj_val(0.1);
			 break;
		 
		 case UI_UINT8_ITEM: case UI_UINT16_ITEM: case UI_INT8_ITEM:
		 case UI_INT16_ITEM: case UI_UINT32_ITEM: case UI_INT32_ITEM:
			 ui_inc_cursor_obj_val(1);
			 break;
		 
		 case UI_CHECK_BOX_ITEM:
			 ui_set_check_box_value(&motor_item, 1);
			 break;
		 
		 default:
			 break;
	 }
}

void ui_sub(void)
{
	 ui_type_t cursor_type = ui_get_cursor_obj_type();
	 switch (cursor_type){
		 case UI_FLOAT_ITEM:
			 ui_inc_cursor_obj_val(-0.1);
			 break;
		 
		 case UI_UINT8_ITEM: case UI_UINT16_ITEM: case UI_INT8_ITEM:
		 case UI_INT16_ITEM: case UI_UINT32_ITEM: case UI_INT32_ITEM:
			 ui_inc_cursor_obj_val(-1);
			 break;
		 
		 case UI_CHECK_BOX_ITEM:
			 ui_set_check_box_value(&motor_item, 0);
			 break;
		 
		 default:
			 break;
	 }
}

