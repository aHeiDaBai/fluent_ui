#ifndef _FLUENT_UI_CONFIG_H
#define _FLUENT_UI_CONFIG_H

/*屏幕刷新周期（单位:s）*/
#define OLED_REFRESH_CYCLE          0.05

/*圆角矩形光标四个角的圆度*/
#define CURSOR_RECTANGLE_ROUNDED    0.5

/*菜单最大深度*/
#define MAX_STACK_LENGTH            3

/*菜单名称距离OLED左侧的像素个数*/
#define OBJ_NAME_DISTANCE_LEFT      10

/*光标圆角矩形的长(指Y轴方向上的长度)*/
#define RECTANGLE_LENGTH            18

/*圆角矩形光标距离左侧的像素个数*/
#define RECTANGLE_DISTANCE_LEFT     5

/*右侧滑杆距离OLED右侧的像素个数*/
#define SLIDER_DISTANCE_RIGHT       4

/*右侧滑杆上正方形方块的宽*/
#define SLIDER_WIDTH                6

/*check box最外面方框的宽*/
#define CHECK_BOX_OUT_WIDTH         10

/*check box方框的宽度*/
#define CHECK_BOX_THICKNESS         2

/*check box勾选时内侧正方形的宽*/
#define CHECK_BOX_IN_WIDTH          4

/*check box和滑杆之间的距离*/
#define CHECK_BOX_DIS_SLIDER        10

/*右侧滑杆上正方形方块的起始位置*/
#define SLIDER_START_POS_X          (OLED_COLUMN - SLIDER_DISTANCE_RIGHT - SLIDER_WIDTH / 2)

/*进入SELECT_ELEMENT后左侧长条的宽度*/
#define LEFT_STRIP_WIDTH            5

/*进入SELECT_ELEMENT后左侧长条的长度*/
#define LEFT_STRIP_LENGTH            60

/*进入SELECT_ELEMENT后左侧长条距离字符串的长度*/
#define LEFT_STPIP_DISTANCE          10

/*圆角矩形光标位置二阶系统的 阻尼比、自然震荡频率、采样周期*/
#define POSITION_DSO_SYS_ZETA       1
#define POSITION_DSO_SYS_OMEGA      20
#define POSITION_DSO_SYS_T          (OLED_REFRESH_CYCLE)

/*圆角矩形光标大小二阶系统的 阻尼比、自然震荡频率、采样周期*/
#define WIDTH_DSO_SYS_ZETA          1
#define WIDTH_DSO_SYS_OMEGA         20
#define WIDTH_DSO_SYS_T             (OLED_REFRESH_CYCLE)

/*右侧滑杆二阶系统的 阻尼比、自然震荡频率、采样周期*/
#define SLIDER_DSO_SYS_ZETA         1
#define SLIDER_DSO_SYS_OMEGA        20
#define SLIDER_DSO_SYS_T            (OLED_REFRESH_CYCLE)

/*obj的name向右滑动的二阶系统的 阻尼比、自然震荡频率、采样周期*/
#define NAME_DSO_SYS_ZETA           1
#define NAME_DSO_SYS_OMEGA          10
#define NAME_DSO_SYS_T              (OLED_REFRESH_CYCLE)

#endif /*_FLUENT_UI_CONFIG_H*/
