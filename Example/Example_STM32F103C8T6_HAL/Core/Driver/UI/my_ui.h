#ifndef MY_UI_H
#define MY_UI_H

#include "fluent_ui.h"

void ui_init(void);
void ui_add(void);
void ui_sub(void);

#endif /*MY_UI_H*/

