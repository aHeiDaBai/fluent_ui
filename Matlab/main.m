clear; clc; close all;
s = tf('s');
omega_n = 30;  % 自然震荡频率
zeta = 0.35;      % 阻尼比
T = 0.05;      % 采样周期(s)
%原系统响应
sys_continus = omega_n*omega_n/(s^2+2*zeta*omega_n*s+omega_n*omega_n);
figure; step(sys_continus); grid on; title("continuous system");
sys_decreate = c2d(sys_continus, T, 'tustin');
figure; step(sys_decreate); grid on;  title("step response");
%利用差分方程迭代法求解
%差分方程参数
a0 = 4+4*zeta*omega_n*T + omega_n*omega_n*T*T;
a1 = 2*omega_n*omega_n*T*T -8;
a2 = 4-4*zeta*omega_n*T + omega_n*omega_n*T*T;
b0 = omega_n*omega_n*T*T;
b1 = 2 * b0;
b2 = b0;
%迭代求解差分方程的值
r = ones(1, 30) * 1;  % 此处修改输入（包括幅值等）
c = zeros(1, length(r));
c(1) = 0; % 初值
c(2) = 0; % 初值
for i = 3: length(r)
    c(i) = (b0*r(i) + b1*r(i-1) + b2*r(i-2) - a2*c(i-2) - a1*c(i-1))/a0;
end
figure;
time = 0:T:T * 29;
stairs(time, c); grid on; title("difference equation");

%%
% 快速且无超调：
omega_n = 20;
zeta = 1;
T = 0.05;
% 慢速无超调
omega_n = 10;
zeta = 1;
T = 0.05;

