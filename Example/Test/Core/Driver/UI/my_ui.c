#include <string.h>

#include "my_ui.h"

static ui_obj_t show_page, null_page, change_page, str_page, special_page;

/*show界面*/
static ui_obj_t uint8_item, int8_item, uint16_item, int16_item, uint32_item, int32_item;
static ui_obj_t float1_item, float2_item;
/*测试——show界面变量*/
uint8_t u8_dat = 244; uint16_t u16_dat = 65535; uint32_t u32_dat = 123456;
int8_t int8_dat = -120; int16_t int16_dat = -289; int32_t int32_dat = -65578;
float float_dat1 = -839.02914, float_dat2 = 912389.9341098;

/*str界面*/
static ui_obj_t str1_item;

/*str界面的变量*/
static char str1[10];

/*界面深度测试*/
static ui_obj_t depth1, depth2, depth3, depth4, depth5;

/*special测试：一些特殊情况的测试，包括字符串超出屏幕大小等*/
static ui_obj_t name_long_page;
//name_long_item, float_long_item, uint16_t_long_item;

void ui_init(void)
{
/*主界面*/
ui_add_page(&show_page,   "+ SHOW",      &first_page, 0);
ui_add_page(&change_page, "+ CHANGE",    &first_page, 1);
ui_add_page(&str_page,    "+ STR",       &first_page, 2);
ui_add_page(&depth1,      "+ DEPTH_TEST",&first_page, 3);
ui_add_page(&special_page,"+ SPECIAL",   &first_page, 4);
ui_add_page(&null_page,   "+ NULL",      &first_page, 5); /*空页面测试*/

/*整形和浮点的展示（show)界面测试（不包括加减测试）*/
ui_add_item(&uint8_item, "UINT8", &show_page, 0, UI_UINT8_ITEM, &u8_dat,     0);
ui_add_item(&int8_item,  "INT8",  &show_page, 1, UI_INT8_ITEM,  &int8_dat,   0);
ui_add_item(&uint16_item,"UINT16",&show_page, 2, UI_UINT16_ITEM,&u16_dat,    0);
ui_add_item(&int16_item, "INT16", &show_page, 3, UI_INT16_ITEM, &int16_dat,  0);
ui_add_item(&uint32_item,"UINT32",&show_page, 4, UI_UINT32_ITEM,&u32_dat,    0);
ui_add_item(&int32_item, "INT32", &show_page, 5, UI_INT32_ITEM, &int32_dat,  0);
ui_add_item(&float1_item,"F1",    &show_page, 6, UI_FLOAT_ITEM, &float1_item,4);
ui_add_item(&float2_item,"F2",    &show_page, 7, UI_FLOAT_ITEM, &float2_item,5);

/*整形和浮点的改变（change)界面测试（包括加减测试）*/
ui_add_item(&uint8_item, "#UINT8", &change_page, 0, UI_UINT8_ITEM, &u8_dat,     0);
ui_add_item(&int8_item,  "#INT8",  &change_page, 1, UI_INT8_ITEM,  &int8_dat,   0);
ui_add_item(&uint16_item,"#UINT16",&change_page, 2, UI_UINT16_ITEM,&u16_dat,    0);
ui_add_item(&int16_item, "#INT16", &change_page, 3, UI_INT16_ITEM, &int16_dat,  0);
ui_add_item(&uint32_item,"#UINT32",&change_page, 4, UI_UINT32_ITEM,&u32_dat,    0);
ui_add_item(&int32_item, "#INT32", &change_page, 5, UI_INT32_ITEM, &int32_dat,  0);
ui_add_item(&float1_item,"#F1",    &change_page, 6, UI_FLOAT_ITEM, &float_dat1, 4);
ui_add_item(&float2_item,"#F2",    &change_page, 7, UI_FLOAT_ITEM, &float_dat2, 5);
	
/*str界面的测试*/
//ui_add_item();
strcpy(str1, "STR1");
ui_add_item(&str1_item,"STR1", &str_page, 0, UI_STRING, str1, 0);

/*界面深度测试*/
/*MAX_STACK_LENGTH宏为3，故最多进入DEPTH4, 无法进入DEPTH5*/
ui_add_page(&depth2, "+ DEPTH2", &depth1,     0);
ui_add_page(&depth3, "+ DEPTH3", &depth2,     0);
ui_add_page(&depth4, "+ DEPTH4", &depth3,     0);
ui_add_page(&depth5, "+ DEPTH5", &depth4,     0);

/*specal界面测试*/
ui_add_page(&name_long_page, "+ NAME_LONG_LONG_LONG_LONG_LONG_LONG", &special_page, 0);



/*最后调用ui_settings_init函数*/
/*注意：必须在界面全部设计完后才可以调用ui_settings_init函数*/
ui_settings_init();
}

void ui_add(void)
{
	 ui_type_t cursor_type = ui_get_cursor_obj_type();
	 switch (cursor_type){
		 case UI_FLOAT_ITEM:
			 ui_inc_cursor_obj_val(0.1);
			 break;
		 
		 case UI_UINT8_ITEM: case UI_UINT16_ITEM: case UI_INT8_ITEM:
		 case UI_INT16_ITEM: case UI_UINT32_ITEM: case UI_INT32_ITEM:
			 ui_inc_cursor_obj_val(1);
			 break;
		 
		 case UI_CHECK_BOX_ITEM:
			 //ui_set_check_box_value(&motor_item, 1);
			 break;
		 
		 default:
			 break;
	 }
}

void ui_sub(void)
{
	 ui_type_t cursor_type = ui_get_cursor_obj_type();
	 switch (cursor_type){
		 case UI_FLOAT_ITEM:
			 ui_inc_cursor_obj_val(-0.1);
			 break;
		 
		 case UI_UINT8_ITEM: case UI_UINT16_ITEM: case UI_INT8_ITEM:
		 case UI_INT16_ITEM: case UI_UINT32_ITEM: case UI_INT32_ITEM:
			 ui_inc_cursor_obj_val(-1);
			 break;
		 
		 case UI_CHECK_BOX_ITEM:
			 //ui_set_check_box_value(&motor_item, 0);
			 break;
		 
		 default:
			 break;
	 }
}

