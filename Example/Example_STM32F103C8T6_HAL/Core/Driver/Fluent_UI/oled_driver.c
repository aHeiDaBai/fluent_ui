#include <string.h>
//SSD1306

#include "oled_driver.h"
#include "oled_font.h"

/**************************
* 8 X 128  共8行，每行128个竖棍
****************************/

static uint8_t oled_buffer[OLED_ROW][OLED_COLUMN];
static uint8_t oled_mask[OLED_ROW][OLED_COLUMN];
static uint8_t oled_output[OLED_ROW][OLED_COLUMN];

/************************************************************************************
* Function: [oled_send_byte]向OLED屏幕发送一个字节
* 
* Input: 要发送的字节
*
* Output: 发送的状态，0为发送失败，1为发送成功
* 
* Warnings: None
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
uint8_t oled_send_byte(uint8_t data)
{
	reset_cs_state();
	//delay_ms(2);
	if(oled_send_buffer(&data, 1, 1000) != HAL_OK){
		set_cs_state();
		return 0;
	}
	//delay_ms(2);
	set_cs_state();
	return 1;
}

/************************************************************************************
* Function: [oled_send_cmd]向OLED屏幕发送一条命令
* 
* Input: 要发送的命令
*
* Output: 发送的状态，0为发送失败，1为发送成功
* 
* Warnings: None
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
uint8_t oled_send_cmd(uint8_t cmd)
{
	reset_cs_state();
	reset_dc_state();
	//while(hspi1.Instance->SR != 0x02);
	if(oled_send_buffer(&cmd, 1, 1000) != HAL_OK){
	    set_dc_state();
	    set_cs_state();
		return 0;
	}
	//delay_ms(2);
	set_dc_state();
	set_cs_state();
	return 1;
}

/************************************************************************************
* Function: [oled_set_pos]设定OLED屏幕开始显示的位置
* 
* Input: [x] 从0到7共八行
*        [y] 从1到127共128列
*
* Output: void
* 
* Warnings: None
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
static void oled_set_pos(uint8_t x, uint8_t y)
{
	if(x > 127 || y > 7)    return;
	
	/*指令0xB0-0xB7用于指定显示的行*/
	oled_send_cmd(0xB0 + y);
	oled_send_cmd(((x & 0xF0) >> 4) | 0x10);
	oled_send_cmd((x & 0x0F) | 0x01);
}

/************************************************************************************
* Function: [oled_refresh]将缓存中的数据发送到屏幕显存中完成屏幕的刷新
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
void oled_refresh(void)
{
	oled_set_pos(0, 0);
	reset_cs_state();
	set_dc_state();
	//delay_ms(2);
	//while(hspi1.Instance->SR != 0x02);
	oled_send_buffer((uint8_t*)oled_output, OLED_ROW * OLED_COLUMN, 1000);
	//delay_ms(1);
	set_cs_state();
}

/************************************************************************************
* Function: [oled_show_mask] 将蒙版中的内容在OLED上展示出来
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void oled_show_mask(void)
{
	oled_set_pos(0, 0);
	reset_cs_state();
	set_dc_state();
	//delay_ms(2);
	//while(hspi1.Instance->SR != 0x02);
	oled_send_buffer((uint8_t*)oled_mask, OLED_ROW * OLED_COLUMN, 1000);
	//delay_ms(1);
	set_cs_state();
}

/************************************************************************************
* Function: [oled_buffer_clear] 清空OLED的缓冲区
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void oled_clear_buffer(void)
{
	memset(oled_buffer, 0, OLED_ROW * OLED_COLUMN);
}

/************************************************************************************
* Function: [oled_mask_clear] 清空OLED的蒙版
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void oled_clear_mask(void)
{
	memset(oled_mask, 0, OLED_ROW * OLED_COLUMN);
}

/************************************************************************************
* Function: [oled_buffer_apply_mask] 给缓冲区添加蒙版的效果
*
* Warnings: None
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void oled_buffer_apply_mask(void)
{
	for(uint8_t i = 0; i < OLED_ROW; ++i){
		for(uint8_t j = 0; j < OLED_COLUMN; ++j){
			oled_output[i][j] = oled_mask[i][j] ^ oled_buffer[i][j];
		}
	}
}

/************************************************************************************
* Function: [oled_clear]清空OLED画面中的内容
* 
* Input: void
*
* Output: void
* 
* Warnings: None
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
void oled_clear(void)
{
	memset(oled_output, 0, OLED_ROW * OLED_COLUMN);
	oled_refresh();
}

/************************************************************************************
* Function: [oled_draw_point]在缓冲区中点亮其中的一个点
* 
* Input: [x, y] 指定此点的位置(x0:0-127, y0:0-63)
*
* Output: void
* 
* Warnings: 1.需要进行一次oled_refresh()才会在屏幕上生效
*           2.此函数不改变缓冲区中其他的内容 
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
void oled_draw_point(uint8_t x, uint8_t y)
{
	if(x > 127 || y > 63)    return;
	/*y/8求出了所在列的索引，y%8求出了所在这一列八个像素点中需要点亮的像素点的索引*/
	//OLED_DRAW_POINT(x, y);
	oled_buffer[y/8][x] |= 0x01 << (y % 8);
}

void clear_square_buffer(uint8_t x0, uint8_t y0, uint8_t length, uint8_t width)
{
	for(uint8_t i = 0; i < length; ++i){
		for(uint8_t j = 0; j < width; ++j){
			oled_buffer[(y0 + j) / 8][x0 + i] &= ~(0x01 << ((y0 + j) % 8));
		}
	}	
}

/************************************************************************************
* Function: [clear_right_part_buffer]清空buffer右侧的内容
* 
* Input: [boundaries] 清除boundaries右侧的内容（包括boundaries）
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
void clear_right_part_buffer(uint8_t boundaries)
{
	if(boundaries > OLED_COLUMN - 1)    return;
	for(uint8_t i = 0; i < OLED_ROW; ++i){
		memset(&oled_buffer[i][boundaries], 0, OLED_COLUMN - boundaries + 1);
	}
}

void clear_right_part_mask(uint8_t boundaries)
{
	if(boundaries > OLED_COLUMN - 1)    return;
	for(uint8_t i = 0; i < OLED_ROW; ++i){
		memset(&oled_mask[i][boundaries], 0, OLED_COLUMN - boundaries + 1);
	}
}

/************************************************************************************
* Function: [oled_draw_line] 在缓冲区中绘制一条直线
* 
* Input: [x1, y1] 直线起始点的坐标(x1:0-127, y1:0-63)
*        [x2, y2] 直线终止点的坐标(x2:0-127, y2:0-63)
*
* Output: void
* 
* Warnings: 1.需要进行一次oled_refresh()才会在屏幕上生效
*           2.此函数不改变缓冲区中其他的内容 
*           目前有点小bug（oled_draw_line(0, 90, 128, 0);）
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
void oled_draw_line(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2)
{
	if(x1 > x2){ /*保证x1 < x2*/
		uint8_t temp = x2;
		x2 = x1;
		x1 = temp;
		temp = y2;
		y2 = y1;
		y1 = temp;
	}
	
	if(x1 > 127)     x1 = 127;
	if(x2 > 127)     x2 = 127;
	if(y1 > 63)      y1 = 63;
	if(y2 > 63)      y2 = 63;
	
	uint8_t x_dis = x2 - x1; /*最大为128，用uint8足够*/
	int8_t  y_dis = y2 - y1;  /*最大为64，用int8足够*/
	
	if(x_dis == 0){
		if(y1 > y2){
			uint8_t temp = y2;
			y2 = y1;
			y1 = temp;
		}
		for(uint8_t i = y1; i <= y2; ++i){
			oled_buffer[i/8][x1] |= 0x01 << (i % 8);
		}
		return;
	}
	
	float inc = (float)y_dis / x_dis;
	
	uint8_t point_already = 0;
	uint8_t y_thory = y1;
	for(uint8_t i = x1; i <= x2; ++i){
	    y_thory = (uint8_t)(y1 + point_already * inc);
		if(y_thory > 63){
			y_thory = 63;
		}
		
		oled_buffer[y_thory/8][i] |= 0x01 << (y_thory % 8);
		point_already ++;
	}
}

/************************************************************************************
* Function: [oled_draw_rectangle] 在蒙版中绘制一个实心的矩形
* 
* Input: [x0, y0] 指定矩形的起始位置(x0:0-127, y0:0-63)
*        [length, width] 指定矩形的长度和宽度(length:1-128, width:1-64)
*
* Output: void
* 
* Warnings: 此函数只在蒙版中起作用不对缓冲区起作用
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void oled_draw_rectangle(uint8_t x0, uint8_t y0, uint8_t length, uint8_t width, uint8_t buffer_select)
{
	/*计算 x轴起始位置：x0，x轴终止位置：x0+length-1*/
	uint8_t column_begin = x0;
	uint8_t column_end = x0 + length - 1;
	if(column_end > 127)    column_end = 127; /*防止超出边界*/
	
	/*计算 y轴起始位置：y0，y轴终止位置：y0 + width - 1*/
	uint8_t row_index_begin = y0 / 8; /*计算起始的在八行之中的第几行*/
	uint8_t row_pos_begin   = y0 % 8; /*计算起始的是当前行的第几个*/
	
	uint8_t y0_end = y0 + width - 1;
	if(y0_end > 63)    y0_end = 63; /*防止超出边界*/
	
	uint8_t row_index_end = y0_end / 8; /*计算结束的在八行之中的第几行*/
	uint8_t row_pos_end   = y0_end % 8; /*计算结束的是当前行的第几个*/
	
	/*先将最上面和最下面的进行填充*/
    //uint8_t up_fill   = 0xff << row_pos_begin;
	//uint8_t down_fill = 0xff >> (7 - row_pos_end);
	//memset(oled_buffer[row_index_begin], up_fill, column_end - column_begin + 1);
	//memset(oled_buffer[row_index_end], down_fill, column_end - column_begin + 1);
	
	if(buffer_select == 0){
		for(uint8_t i = column_begin; i <= column_end; ++i){
			oled_mask[row_index_begin][i] |= 0xff << row_pos_begin;
			oled_mask[row_index_end][i] |= 0xff >> (7 - row_pos_end);
		}
		/*将中间的进行填充*/
		for(uint8_t i = row_index_begin + 1; i < row_index_end; ++i){
			for(uint8_t j = column_begin; j <= column_end; ++j){
				oled_mask[i][j] = 0xff;
			}
		}
    }else{
		for(uint8_t i = column_begin; i <= column_end; ++i){
			oled_buffer[row_index_begin][i] |= 0xff << row_pos_begin;
			oled_buffer[row_index_end][i] |= 0xff >> (7 - row_pos_end);
		}
		/*将中间的进行填充*/
		for(uint8_t i = row_index_begin + 1; i < row_index_end; ++i){
			for(uint8_t j = column_begin; j <= column_end; ++j){
				oled_buffer[i][j] = 0xff;
			}
		}	
	}
}

/************************************************************************************
* Function: [oled_draw_rounded_rectangle] 在蒙版中绘制一个实心的圆角矩形
* 本函数的部分思路参考了以下视频：
* 1. https://www.bilibili.com/video/BV1pG411C7xP/?share_source=copy_web
*    原作者：【大明狐】（BiliBili）
* 
* Input: [x0, y0] 指定圆角矩形的起始位置(x0:0-127, y0:0-63)
*        [length, width] 指定圆角矩形的长度和宽度(length:1-128, width:1-64)
*        [round] 指定圆角矩形四角的圆度（0-1），round越大，圆角矩形越圆
*
* Output: void
* 
* Warnings: 此函数只在蒙版中起作用不对缓冲区起作用
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
#define OLED_DRAW_POINT_MASK(x, y)      (oled_mask[(y)/8][(x)] |= 0x01 << ((y) % 8))
#define OLED_DRAW_POINT_BUFFER(x, y)    (oled_buffer[(y)/8][(x)] |= 0x01 << ((y) % 8))
void oled_draw_rounded_rectangle(uint8_t x0, uint8_t y0, uint8_t length, uint8_t width, float round, uint8_t buffer_select)
{
	if(round < 0)    return;
	if(round > 1)    round = 1;
	if(x0 > 127)     x0 = 127;
	if(y0 > 63)      y0 = 63;

	/*得到矩形四个角的坐标值*/
	uint8_t x_start = x0;
	uint8_t x_end   = x0 + length - 1;
	if(x_end > 127)    x_end = 127;
	
	uint8_t y_start = y0;
	uint8_t y_end   = y0 + width - 1;
	if(y_end > 63)    y_end = 63;
	
	/*求半径长度，半径长度不得小于最小边的一半*/
	uint8_t radius = 0;
	if(length > width)    radius = width / 2 * round;
	else                  radius = length / 2 * round;
	if(radius == 0)    return;

    uint8_t x = 0;
	uint8_t y = radius;
	int16_t d = 3 - 2 * radius;
    /*绘制上下两段拱形*/
	if(buffer_select == 0){
		while(x <= y){
			for(uint8_t i = x_start + radius - x; i <= x_end - radius + x; ++i){
				OLED_DRAW_POINT_MASK(i, y_start + radius - y);
				OLED_DRAW_POINT_MASK(i, y_end - radius + y);
			}
			
			for(uint8_t i = x_start + radius - y; i <= x_end - radius + y; ++i){
				OLED_DRAW_POINT_MASK(i, y_start + radius - x);
				OLED_DRAW_POINT_MASK(i, y_end - radius + x);
			}
			
			if(d < 0){
				d += 4 * x + 6;
			}else{
				d += 4 * (x - y) + 10;
				y--;
			}
			x++;
		}
	}else{
		while(x <= y){
			for(uint8_t i = x_start + radius - x; i <= x_end - radius + x; ++i){
				OLED_DRAW_POINT_BUFFER(i, y_start + radius - y);
				OLED_DRAW_POINT_BUFFER(i, y_end - radius + y);
			}
			
			for(uint8_t i = x_start + radius - y; i <= x_end - radius + y; ++i){
				OLED_DRAW_POINT_BUFFER(i, y_start + radius - x);
				OLED_DRAW_POINT_BUFFER(i, y_end - radius + x);
			}
			
			if(d < 0){
				d += 4 * x + 6;
			}else{
				d += 4 * (x - y) + 10;
				y--;
			}
			x++;
		}
	}
	/*填充中间的矩形部分*/
	oled_draw_rectangle(x_start, y_start + radius + 1, length, width - 2 * radius, buffer_select);
}

/************************************************************************************
* Function: [oled_write_char] 在缓冲区中写入一个F6X8大小的字符
*
* Input: [x, y] 指定字符的起始位置(x0:0-121, y0:0-6)
*        [data] 要写入的字符
*
* Output: void
* 
* Warnings: 1.需要进行一次oled_refresh()才会在屏幕上生效
*           2.此函数不改变缓冲区中其他的内容 
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void oled_write_char_F6X8(uint8_t x, uint8_t y, char data)
{   
	if(data < ' ' || data > 'z')    return;
	if(y > 6 || x > 121)    return;

	for(uint8_t i = 0; i < 6; i++)
		oled_buffer[y+1][x + i] = F6x8[(uint8_t)(data - ' ')][i];
}

void oled_write_char_F8X16(uint8_t x, uint8_t y, char data)
{   
	if(data < ' ' || data > 'z')    return;
	if(y > 6 || x > 121)    return;
	
	uint8_t index = data - ' ';
	for(uint8_t i = 0; i < 8; i++)
		oled_buffer[y][x + i] = F8X16[index * 16 + i];
	for(uint8_t i = 0; i < 8; i++)
		oled_buffer[y+1][x + i] = F8X16[index * 16 + i + 8];
}

/************************************************************************************
* Function: [oled_write_string_F8X16] 在缓冲区中写入一个F8X16大小的字符串
*
* Input: [x, y] 指定字符串的起始位置(x0:0-127, y0:0-6)
*        [str]  要写入的字符串首地址
*
* Output: void
* 
* Warnings: 1.需要进行一次oled_refresh()才会在屏幕上生效
*           2.此函数不改变缓冲区中其他的内容 
*           3.此函数的输入x可以为负值，此时函数会将x大于0的字符写入缓冲区
*
* History:    2024-01-14 AHeiDaBai : First version
*             2024-01-28 AHeiDaBai : 修复了超出屏幕左界就不显示的bug
*==================================================================================*/
void oled_write_string_F8X16(int16_t x, uint8_t y, char * str)
{
	int16_t x_pos = 0;
	for(uint8_t index = 0; str[index] != '\0'; ++index){
		x_pos = x + index * 8;
		if(x_pos > 127)    return;
		if(x_pos < 0)   continue; /*直到遇到x>0的数*/
		oled_write_char_F8X16(x_pos, y, str[index]);
	}
}

void oled_write_string_F6X8(int16_t x, uint8_t y, char * str)
{
	int16_t x_pos = 0;
	for(uint8_t index = 0; str[index] != '\0'; ++index){
		x_pos = x + index * 8;
		if(x_pos > 127)    return;
		if(x_pos < 0)   continue; /*直到遇到x>0的数*/
		oled_write_char_F6X8(x_pos, y, str[index]);
	}
}


void oled_hardware_init(void)
{
	/*将屏幕复位*/
    set_res_state();
	fluent_ui_delay_ms(100);
	reset_res_state();
	fluent_ui_delay_ms(200);
	set_res_state();
	
	oled_send_cmd(0xAE);//--turn off oled panel
	oled_send_cmd(0x00);//---set low column address
	oled_send_cmd(0x10);//---set high column address
	oled_send_cmd(0x40);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
	oled_send_cmd(0x81);//--set contrast control register
	oled_send_cmd(0xCF); // Set SEG Output Current Brightness
	oled_send_cmd(0xA1);//--Set SEG/Column Mapping     0xa0左右反置 0xa1正常
	oled_send_cmd(0xC8);//Set COM/Row Scan Direction   0xc0上下反置 0xc8正常
	
	/*设置正常显示还是反色显示 A6:正常 A7:反色*/
	//oled_send_cmd(0xA7);//--set normal display
	oled_send_cmd(0xA6);// Disable Inverse Display On (0xa6/a7) 
	
	oled_send_cmd(0xA8);//--set multiplex ratio(1 to 64)
	oled_send_cmd(0x3f);//--1/64 duty
	oled_send_cmd(0xD3);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
	oled_send_cmd(0x00);//-not offset
	oled_send_cmd(0xd5);//--set display clock divide ratio/oscillator frequency
	oled_send_cmd(0x80);//--set divide ratio, Set Clock as 100 Frames/Sec
	oled_send_cmd(0xD9);//--set pre-charge period
	oled_send_cmd(0xF1);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	oled_send_cmd(0xDA);//--set com pins hardware configuration
	oled_send_cmd(0x12);
	oled_send_cmd(0xDB);//--set vcomh
	oled_send_cmd(0x40);//Set VCOM Deselect Level
	
	/*设定地址模式为页地址模式*/
	oled_send_cmd(0x20);//-Set Page Addressing Mode (0x00/0x01/0x02)
	oled_send_cmd(0x00);/*00H:水平地址模式，01H：垂直地址模式，02H：页地址模式*/
	
	oled_send_cmd(0x8D);//--set Charge Pump enable/disable
	oled_send_cmd(0x14);//--set(0x10) disable
	oled_send_cmd(0xA4);// Disable Entire Display On (0xa4/0xa5)
	
	oled_send_cmd(0xAF);//--turn on oled panel
		
	oled_clear();
}
