#include <math.h>
#include <stdio.h>
#include <string.h>

#include "fluent_ui.h"
#include "oled_driver.h"
#include "fluent_ui_config.h"
#include "discrete_second_order_system.h"

#define OBJ_NAME_NUMBER    4
#define MAX_STR_LEN        15

typedef struct cursor_s
{
    uint8_t pos;    //当前页面的第几个obj，不等于id
	uint8_t absolute_pos;  /*所指向的obj在page中的绝对位置*/
	
	uint8_t width_expected;  /*期望的宽*/
	uint8_t width_actual;    /*实际的宽*/
	
	uint8_t length_expected; /*期望的长（X轴方向）*/
	uint8_t length_actual;   /*实际的长（X轴方向）*/
	
	uint8_t y_pos_actual;    /*实际的Y轴坐标*/
	uint8_t y_pos_expected;  /*期望的Y轴坐标*/
	
	uint8_t x_pos_actual;    /*实际的X轴坐标*/
	uint8_t x_pos_expected;  /*期望的X轴坐标*/
	
	uint8_t enter_select_element_flag;
	
    ui_obj_t *obj;  /*当前光标指向的obj*/
    ui_obj_t *page; /*光标当前所在的页*/
	
}cursor_t;

typedef struct ui_stack_s{
    uint8_t top;
    cursor_t base[MAX_STACK_LENGTH];
}ui_stack_t;

ui_obj_t first_page = {
        .next = NULL,
        .prior = NULL,
        .name = "FIRST PAGE",
        .priv_charac.page_charac.start = NULL,
	    .priv_charac.page_charac.item_num = 0,
        .type = UI_PAGE,
        .id = 0,
};

static uint8_t  boundaries_expected = OLED_COLUMN - 1;
static uint16_t identity_id;

/*页面光标*/
static cursor_t ui_cursor;
/*页面进出使用的栈*/
static ui_stack_t ui_stack;
/*页面动画*/
static dso_system_t x_pos_dso, y_pos_dso;
static dso_system_t width_dso, length_dso;
static dso_system_t slider_dso;
static dso_system_t mask_dso;
static dso_system_t name_dso[OBJ_NAME_NUMBER];  /*对应屏幕上四行obj的名称*/

/************************************************************************************
* Function: [ui_draw_check_box] 绘制check box
*
* Input: [self] 表示此函数是作为ui_obj_t的一个方法使用的
*        [pos]  取值在（0-3）中，表示了check box的y坐标
* 
* Warnings: 此函数挂载到draw_element函数指针下，作为方法被调用
*
* History:    2024-01-15 AHeiDaBai : First version
*==================================================================================*/
static void ui_draw_check_box(ui_obj_t *self, uint8_t pos)
{
	if(self->type != UI_CHECK_BOX_ITEM)    return;
	
	/*外层的框*/
	static uint8_t left_xstart = OLED_COLUMN - SLIDER_DISTANCE_RIGHT - SLIDER_WIDTH / 2 - CHECK_BOX_DIS_SLIDER - CHECK_BOX_OUT_WIDTH;
	uint8_t left_ystart = pos * 16  + (16 - SLIDER_WIDTH) / 2;
	oled_draw_rounded_rectangle(left_xstart, left_ystart, CHECK_BOX_OUT_WIDTH, CHECK_BOX_OUT_WIDTH, 0.5, 1);
	
	/*扣除中间的部分*/
	clear_square_buffer(left_xstart + CHECK_BOX_THICKNESS,               \
	                    left_ystart + CHECK_BOX_THICKNESS,               \
	                    CHECK_BOX_OUT_WIDTH - 2 * CHECK_BOX_THICKNESS,   \
                        CHECK_BOX_OUT_WIDTH - 2 * CHECK_BOX_THICKNESS);

	/*绘制中间的实心正方形*/
	if(self->priv_charac.item_charac.val != 0){
		oled_draw_rectangle(left_xstart + (CHECK_BOX_OUT_WIDTH - CHECK_BOX_IN_WIDTH) / 2,   \
	                        left_ystart + (CHECK_BOX_OUT_WIDTH - CHECK_BOX_IN_WIDTH) / 2,   \
	                        CHECK_BOX_IN_WIDTH,                                             \
	                        CHECK_BOX_IN_WIDTH, 1);
	}
}

/************************************************************************************
* Function: [ui_get_check_box_value] 获取check box的值
*
* Input: [item] check box所在的ui_obj_t结构体指针
* 
* Output: check box的值
*
* History:    2024-01-15 AHeiDaBai : First version
*==================================================================================*/
uint8_t ui_get_check_box_value(ui_obj_t *item)
{
	if(item == NULL)    return 2;
	if(item->type != UI_CHECK_BOX_ITEM)    return 2;
	
	return (item->priv_charac.item_charac.val);
}

/************************************************************************************
* Function: [ui_get_check_box_value] 设定check box的值
*
* Input: [item] check box所在的ui_obj_t结构体指针
*        [new_val] check box的新值
*
* History:    2024-01-15 AHeiDaBai : First version
*==================================================================================*/
void ui_set_check_box_value(ui_obj_t *item, uint8_t new_val)
{
	if(item == NULL)    return;
	if(item->type != UI_CHECK_BOX_ITEM)    return;
	
	if(new_val != 0){
		item->priv_charac.item_charac.val = 1;
	}else{
		item->priv_charac.item_charac.val = 0;
	}
}

/************************************************************************************
* Function: [ui_show_string] UI_STRING类型绘制字符串的方法
*
* Input: [self] 表示此函数是作为ui_obj_t的一个方法使用的
*        [pos]  取值在（0-3）中，表示了string的y坐标
* 
* Warnings: 此函数挂载到draw_element函数指针下，作为方法被调用
*
* History:    2024-02-11 AHeiDaBai : First version
*==================================================================================*/
static void ui_show_string(ui_obj_t *self, uint8_t pos)
{
	static uint8_t x_end = OLED_COLUMN - SLIDER_DISTANCE_RIGHT - SLIDER_WIDTH - CHECK_BOX_DIS_SLIDER;

	if(self->type != UI_STRING)    return;
	
	char *str = self->priv_charac.item_charac.data;
	if(str == NULL || pos > OBJ_NAME_NUMBER - 1)    return;
	
	uint8_t str_len = strlen(str);
	if(str_len > MAX_STR_LEN)    str_len = MAX_STR_LEN;
	
	uint8_t x_start = x_end - str_len * 6;  /*F6X8的字符的宽度为6*/
	oled_write_string_F6X8(x_start, pos * 2, str);
}

/************************************************************************************
* Function: [ui_get_cursor_identity_id] 获取光标所指的obj的identity_id
*
* Output: 光标所指的obj的identity_id（0或1）
*
* Warning: 当获取identity_id失败时，返回0（程序保证了identity_id不会分配0给任何item）
*
* History:    2024-01-15 AHeiDaBai : First version
*==================================================================================*/
uint16_t ui_get_cursor_identity_id(void)
{
	if(ui_cursor.obj == NULL)    return 0;
	if(ui_cursor.obj->type == UI_PAGE)    return 0;
	
	return (ui_cursor.obj->identity_id);
}

/************************************************************************************
* Function: [ui_get_cursor_obj_type] 获取光标所指的obj的type
*
* Output: 光标所指的obj的type（ui_type_t类型）
*
* Example: ui_type_t cursor_obj_type = ui_get_cursor_obj_type();
*
* History:    2024-02-19 AHeiDaBai : First version
*==================================================================================*/
ui_type_t ui_get_cursor_obj_type(void)
{
	if(ui_cursor.obj == NULL)    return UI_ERROR;
	
	return (ui_type_t)(ui_cursor.obj->type);
}

/************************************************************************************
* Function: [ui_inc_cursor_obj_val] 将光标所指的obj的val进行增加
*
* Input: [inc_val] 增加的量
*
* Warning: !!此函数不会对数据的溢出等进行检查!!
*
* History:    2024-02-19 AHeiDaBai : First version
*==================================================================================*/
void ui_inc_cursor_obj_val(float inc_val)
{
	if(ui_cursor.obj == NULL)    return;
	
	ui_type_t type = (ui_type_t)(ui_cursor.obj->type);
	if(type == UI_PAGE   || type == UI_SELECT_ELEMENT ||
	   type == UI_STRING || type == UI_CHECK_BOX_ITEM){
	   return;
	}
	
	/*限制只有name以#开头才可以被修改*/
    if(ui_cursor.obj->name[0] != '#')    return;	
	
	void *data = ui_cursor.obj->priv_charac.item_charac.data;
	switch(type){
		case UI_FLOAT_ITEM:{
			float *data_float = (float*)data;
			*(data_float) = (float)(*(data_float) + inc_val);
			break;
		}
		case UI_INT8_ITEM:{
			int8_t *dat_int8 = (int8_t*)data;
			*(dat_int8) = (int8_t)(*(dat_int8) + inc_val);
			break;
		}
		case UI_INT16_ITEM:{
			int16_t *dat_int16 = (int16_t*)data;
			*(dat_int16) = (int16_t)(*(dat_int16) + inc_val);
			break;
		}
		case UI_INT32_ITEM:{
			int32_t *dat_int32 = (int32_t*)data;
			*(dat_int32) = (int32_t)(*(dat_int32) + inc_val);
			break;
		}
		case UI_UINT8_ITEM:{
			uint8_t *dat_uint8 = (uint8_t*)data;
			*(dat_uint8) = (uint8_t)(*(dat_uint8) + inc_val);
			break;
		}
		case UI_UINT16_ITEM:{
			uint16_t *dat_uint16 = (uint16_t*)data;
			*(dat_uint16) = (uint16_t)(*(dat_uint16) + inc_val);
			break;
		}
		case UI_UINT32_ITEM:{
			uint32_t *dat_uint32 = (uint32_t*)data;
			*(dat_uint32) = (uint32_t)(*(dat_uint32) + inc_val);
			break;
	    }
		default:
			return;
	}
}

/************************************************************************************
* Function: [float_to_str] 将浮点数按照指定的位数转为字符串
*
* Input: [string]     存储结果的字符数组地址
*        [data_f]     要转换的浮点数
*        [precision]  保留小数点的位数
*
* Warning: 用于存储结果的字符数组（即string）大小要合适
*
* History:    2024-01-16 AHeiDaBai : First version
*==================================================================================*/
static void float_to_str(const char *string, float data_f, uint8_t precision)
{
	if(string == NULL)    return;
	
	char *str = (char*)string;
	if(data_f < 0){
		str[0] = '-';
		str ++;
		data_f = -data_f;
	}
	/*处理整数部分*/
	int32_t data_int32 = (int32_t)data_f;
	sprintf(str, "%d", data_int32);
	/*添加小数点*/
	uint8_t integer_len = strlen(str);
	str[integer_len] = '.';
	integer_len ++;
	/*处理小数部分*/
	float data_point = data_f - data_int32;
	uint8_t num_tmp = 0;
	while(precision--){
		data_point = data_point * 10;
		num_tmp = (uint8_t)data_point;
		str[integer_len] = num_tmp + '0';
		integer_len ++;
		data_point -= num_tmp;
	}
}

/************************************************************************************
* Function: [ui_draw_num] 绘制整数或者浮点数
*
* Input: [self] 表示此函数是作为ui_obj_t的一个方法使用的
*        [pos]  取值在（0-3）中，表示了check box的y坐标
* 
* Warnings: 此函数挂载到draw_element函数指针下，作为方法被调用
*
* History:    2024-01-15 AHeiDaBai : First version
*==================================================================================*/
static void ui_draw_num(ui_obj_t *self, uint8_t pos)
{
	static char str_buffer[MAX_STR_LEN]; /*由于屏幕比较小，故可容纳的字符数较少*/
	static uint8_t x_end = OLED_COLUMN - SLIDER_DISTANCE_RIGHT - SLIDER_WIDTH - CHECK_BOX_DIS_SLIDER;
	
	if(self->type == UI_PAGE || self->type == UI_SELECT_ELEMENT || self->type == UI_STRING)    
		return;
		
	void *data = self->priv_charac.item_charac.data;
	if(data == NULL || pos > 3)    
		return;
	
	uint8_t x_start = 0;
	int32_t data_buffer = 0;
	memset(str_buffer, 0, sizeof(str_buffer));
	
	if(self->type == UI_FLOAT_ITEM){
		float_to_str(str_buffer, *((float*)data), self->priv_charac.item_charac.val);
	}else{
		switch(self->type){
			case UI_INT8_ITEM:
				data_buffer = (int32_t)(*((int8_t*)data));
				break;
			case UI_INT16_ITEM:
				data_buffer = (int32_t)(*((int16_t*)data));
				break;
			case UI_INT32_ITEM:
				data_buffer = (int32_t)(*((int32_t*)data));
				break;
			case UI_UINT8_ITEM:
				data_buffer = (int32_t)(*((uint8_t*)data));
				break;
			case UI_UINT16_ITEM:
				data_buffer = (int32_t)(*((uint16_t*)data));
				break;
			case UI_UINT32_ITEM:
				data_buffer = (int32_t)(*((uint32_t*)data));
				break;
			default:
				return;
		}
		sprintf(str_buffer, "%d", data_buffer);
    }
	
	uint8_t str_buffer_len = strlen(str_buffer);
	if(str_buffer_len > MAX_STR_LEN)
		str_buffer_len = MAX_STR_LEN;
	
	x_start = x_end - str_buffer_len * 6;  /*F6X8的字符的宽度为6*/
	oled_write_string_F6X8(x_start, pos * 2, str_buffer);
}

/************************************************************************************
* Function: [ui_draw_select_element] 当类型为select element时，绘制右侧的列表
*
* Input: [self] 表示此函数是作为ui_obj_t的一个方法使用的
*        [pos]  **表示字符串的起始位置(y坐标),通过左侧的长条的位置获取**
* 
* Warnings: 此函数挂载到draw_element函数指针下，作为方法被调用
*
* History:    2024-01-28 AHeiDaBai : First version
*==================================================================================*/
static void ui_draw_select_element(ui_obj_t *self, uint8_t pos)
{
	if(self->type != UI_SELECT_ELEMENT)    return;
	
	char  **str_array = self->priv_charac.item_charac.data;
	uint8_t array_len = self->priv_charac.item_charac.val;
	
	/*对pos进行修正*/
	/*必须满足：前面的元素个数(pos)+后面可拓展的最大元素个数(len-a_p) >= 4*/
	int16_t dif = ui_cursor.pos + array_len - ui_cursor.absolute_pos - 4;
	if(dif < 0){
		ui_cursor.pos = ui_cursor.pos - dif;
		//ui_cursor.absolute_pos = ui_cursor.absolute_pos - dif;
		ui_cursor.y_pos_expected = ui_cursor.pos * 16;
	}
	
	/*求小页面中画面最上方字符串在数组中的索引*/
	int16_t start_index = ui_cursor.absolute_pos - ui_cursor.pos;
	
	/*将字符串绘制到oled buffer*/
	if(array_len > 4)    array_len = 4;
	for(uint8_t i = 0; i < array_len; ++i){
		oled_write_string_F8X16(pos, 2 * i, str_array[start_index + i]);
	}
}

/************************************************************************************
* Function: [ui_add_page] 在某个页面上添加一个page
*
* Input: [page] 要添加的page结构体的地址
*        [name] 要添加的page的名字，用于在界面上进行展示
*        [last_page, id] 说明了要展示在哪个页面的哪个位置
* 
* Warnings: None
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
void ui_add_page(ui_obj_t *page, char *name, ui_obj_t *last_page, uint8_t id)
{
    if(!page || !last_page || !name)    return;

    page->name  = name;
    page->id    = id;
    page->prior = NULL;
    page->next  = NULL;
    page->type  = UI_PAGE;
	page->identity_id = 0;
	page->name_len = strlen(name);
	page->priv_charac.page_charac.item_num = 0;
	page->priv_charac.page_charac.start = NULL;

    /*================将page进行插入==================*/
    if(!(last_page->priv_charac.page_charac.start)){
        /*last_page下目前还没有元素*/		
		last_page->priv_charac.page_charac.start = page;
    }else{
		/*last_page下至少有一个元素：利用id来查找page的前一个元素*/
        ui_obj_t *before = last_page->priv_charac.page_charac.start;
        while((before->next)){
            if(before->id < id && id < (before->next)->id) break;
            before = before->next;
        }

		/*将page插在before和before->next之间*/
        page->prior = before;
        page->next = before->next;
        before->next = page;
        if(page->next)    (page->next)->prior = page;
		
		last_page->priv_charac.page_charac.item_num ++;
    }
}

/************************************************************************************
* Function: [ui_add_item] 在某个页面上添加一个item
*
* Input: [item] 要添加的item结构体的地址
*        [name] 要添加的item的名字，用于在界面上进行展示
*        [last_page, id] 说明了要展示在哪个页面的哪个位置
*        [data] 要挂载的数据的地址(如果是UI_CHECK_BOX_ITEM类型，则此参数可以任填，如NULL)
*        [precision] 展示多少位小数（只对UI_FLOAT_ITEM浮点类型有效，其余类型可任填，如0）
* 
* Warnings: None
*
* History:    2024-01-13 AHeiDaBai : First version
*==================================================================================*/
void ui_add_item(ui_obj_t *item, char *name, ui_obj_t *last_page, uint8_t id, uint8_t type, void *data, uint8_t precision)
{
    if(!item || !last_page || !name)    return;

    item->name = name;
    item->id = id;
    item->prior = NULL;
    item->next = NULL;
    item->type = type;
	identity_id ++;
	item->name_len = strlen(name);
	item->identity_id = identity_id;
	
	/*data：如果是数值型，如FLOAT、UINT8等，存储的是数据的地址*/
	/*      如果是SELECT_ELEMENT类型的，存储的是字符串数组的地址*/
	item->priv_charac.item_charac.data = data;
	
	/*根据类型选择绘制其的方法*/
	switch (item->type){
		case UI_PAGE:{
			item->priv_charac.item_charac.draw_element = NULL;
			break;
		}
		case UI_CHECK_BOX_ITEM:{
			item->priv_charac.item_charac.draw_element = ui_draw_check_box;
			break;
		}
		case UI_STRING:{
			item->priv_charac.item_charac.draw_element = ui_show_string;
			break;
		}
		case UI_SELECT_ELEMENT:{
			/*字符串数组中最长字符串的长度*/
			uint8_t str_len = 0;
			char ** str_array = (char**)data;
			item->max_len = 0;
			for(uint8_t i = 0; i < precision; ++i){
				str_len = strlen(str_array[i]);
				if(item->max_len < str_len){
					item->max_len = str_len;
				}
			}
			item->priv_charac.item_charac.draw_element = ui_draw_select_element;
			/*UI_SELECT_ELEMENT类型时，存储的是字符串列表的长度*/
			item->priv_charac.item_charac.val = precision;
			break;
	    }
		default:{
			item->priv_charac.item_charac.draw_element = ui_draw_num;
			if(type == UI_FLOAT_ITEM){
				/*UI_FLOAT类型时，precision存储的是小数点的位数*/
				item->priv_charac.item_charac.val = precision;
			}
			break;
		}
	}

	/*================将item进行插入==================*/
    if(!(last_page->priv_charac.page_charac.start)){
        /*last_page下目前还没有元素*/			
		last_page->priv_charac.page_charac.start = item;
    }else{
		/*last_page下至少有一个元素：利用id来查找page的前一个元素*/
        ui_obj_t *before = last_page->priv_charac.page_charac.start;
        while((before->next)){
            if(before->id < id && id < (before->next)->id) break;
            before = before->next;
        }
		/*将item插在before和before->next之间*/
        item->prior = before;
        item->next = before->next;
        before->next = item;
        if((item->next))    (item->next)->prior = item;
		
		last_page->priv_charac.page_charac.item_num ++;
    }
}

/************************************************************************************
* Function: [find_interface_obj_start] 根据光标当前指向的obj和pos查找在当前界面中第一个obj
*           由于此功能在多处调用，因此封装成函数以减少代码量
*
* History:    2024-01-16 AHeiDaBai : First version
*==================================================================================*/
static ui_obj_t* find_interface_obj_start(void)
{
	/*将ui_cursor.obj向前移动ui_cursor.pos次即可得到第一个obj*/
	ui_obj_t *obj_begin = ui_cursor.obj;
	if(obj_begin == NULL)    return NULL;
	
	for(uint8_t i = 0; i < ui_cursor.pos; ++i){
		if(obj_begin != NULL){
		    obj_begin = obj_begin->prior;
		}else{
			return NULL;
		}
	}
	return obj_begin;
}

/************************************************************************************
* Function: [set_obj_name_dso_params] 此函数用于设定name_dso中的相关参数，
*           以实现obj的name间隔向右滑动的效果，
*           由于此功能在多处调用，因此封装成函数以减少代码量
*
* Input: [start] 当前页面的首个obj的地址，即find_interface_obj_start函数的返回值，
*                如果传入NULL，会在函数内部计算
*
* History:    2024-01-16 AHeiDaBai : First version
*==================================================================================*/
static void set_obj_name_dso_params(ui_obj_t *start)
{
	ui_obj_t *obj = start;
	if(start == NULL){
		obj = find_interface_obj_start();
	}
	/*第一个元素进行特殊处理*/
	name_dso[0].last_input = 0;
	name_dso[0].last_last_input = 0;
	name_dso[0].last_output = - obj->name_len * 8;
	name_dso[0].last_last_output = name_dso[0].last_output;

	for(uint8_t i = 1; i < OBJ_NAME_NUMBER; ++i){
		name_dso[i].last_input = 0;
		name_dso[i].last_last_input = 0;
		/*obj的name显示时是F8X16字体，宽度为8*/
		name_dso[i].last_output = name_dso[i-1].last_output - obj->name_len * 8;
		name_dso[i].last_last_output = name_dso[i].last_output;
		if(obj->next != NULL){
			obj = obj->next;
		}else{
			break;
		}
	}
}

/************************************************************************************
* Function: [draw_ui_main_interface] 在缓冲区绘制主界面中的四个字符串
*
* History:    2024-01-16 AHeiDaBai : First version
*==================================================================================*/
static void draw_ui_main_interface(void)
{
	/*在缓冲区中绘制obj的name*/
	ui_obj_t *obj_begin = find_interface_obj_start();
	if(obj_begin != NULL){
		for(uint8_t i = 0; i < 4; ++i){
			int16_t name_x = dso_system_cal(&name_dso[i], OBJ_NAME_DISTANCE_LEFT);
		    oled_write_string_F8X16(name_x, 2 * i, obj_begin->name);
			/*如果是item类型的话，还需要绘制checkbox或者绘制对应的值等*/
			if(obj_begin->type != UI_PAGE && obj_begin->type != UI_SELECT_ELEMENT){
				obj_begin->priv_charac.item_charac.draw_element(obj_begin, i);
			}
			obj_begin = obj_begin->next;
			if(obj_begin == NULL)    return;
	    }
	}
}

/************************************************************************************
* Function: [ui_display_page] UI页面的刷新显示（UI界面的核心函数）
*
* Warnings: ！！此函数需要定期执行！！
*           ！！此函数的执行周期必须要和ui_config.h中的OLED_REFRESH_CYCLE宏一致！！
*
* History:    2024-01-15 AHeiDaBai : First version
*             2024-01-28 AHeiDaBai : 增加了SELECT ELEMENT元素的支持
*==================================================================================*/
void ui_refresh(void)
{
	oled_clear_mask();
	
	/*光标（圆角矩形）期望长度（Y轴方向）*/
	ui_cursor.length_expected = RECTANGLE_LENGTH;
	uint8_t page_item_num;
	if(ui_cursor.enter_select_element_flag == 0){
		/*光标（圆角矩形）期望宽度(X轴方向)*/
		ui_cursor.width_expected = ui_cursor.obj->name_len * 8 + OBJ_NAME_DISTANCE_LEFT;
		page_item_num = ui_cursor.page->priv_charac.page_charac.item_num;
        /*光标（圆角矩形）期望X坐标*/
		ui_cursor.x_pos_expected = RECTANGLE_DISTANCE_LEFT;
		/*绘制主页面的字符串菜单*/
		oled_clear_buffer();
		draw_ui_main_interface();
	}else{
		/*光标（圆角矩形）期望宽度(X轴方向)*/
		char **str_arr = ui_cursor.obj->priv_charac.item_charac.data;
		uint8_t _str_len = strlen(str_arr[ui_cursor.obj->value]);
		ui_cursor.width_expected = _str_len * 8 + CURSOR_RECTANGLE_ROUNDED * ui_cursor.length_expected;
		
		page_item_num = ui_cursor.obj->priv_charac.item_charac.val;
		
		/*计算光标（圆角矩形）期望X坐标*/
        int16_t x_start = SLIDER_START_POS_X - 8 * ui_cursor.obj->max_len - CURSOR_RECTANGLE_ROUNDED * ui_cursor.length_expected / 2;
		if(x_start < 5){  /*防止因字符串过长而导致的超出屏幕外*/
			ui_cursor.x_pos_expected = 5;
		}else{
			ui_cursor.x_pos_expected = x_start;
		}
		
        /*如果是小画面，则进行部分画面的清除（实现小画面的进入）*/
		boundaries_expected = ui_cursor.x_pos_expected - LEFT_STPIP_DISTANCE;
		//uint8_t boundaries_actual = dso_system_cal(&mask_dso, boundaries_expected);
		clear_right_part_buffer(boundaries_expected);
		
		/*根据光标（圆角矩形）期望X坐标来计算左侧分隔栏的X坐标*/
		oled_draw_line(ui_cursor.x_pos_expected - LEFT_STPIP_DISTANCE / 2, 2, ui_cursor.x_pos_expected - LEFT_STPIP_DISTANCE / 2, 62);  /*长60*/
		
		/*调用ui_draw_select_element函数*/
		/*利用boundaries_expected来计算字符串的X轴位置，保证了两者的位置是相对静止的*/
	    //uint8_t str_x_pos = boundaries_actual + LEFT_STPIP_DISTANCE + CURSOR_RECTANGLE_ROUNDED * ui_cursor.length_expected / 2;
		ui_cursor.obj->priv_charac.item_charac.draw_element(ui_cursor.obj, SLIDER_START_POS_X - 8 * ui_cursor.obj->max_len);
	}
	
	/*绘制光标（圆角矩形）*/
	ui_cursor.x_pos_actual  = dso_system_cal(&x_pos_dso,  ui_cursor.x_pos_expected);
	ui_cursor.y_pos_actual  = dso_system_cal(&y_pos_dso,  ui_cursor.y_pos_expected);
	ui_cursor.width_actual  = dso_system_cal(&width_dso,  ui_cursor.width_expected);
	ui_cursor.length_actual = dso_system_cal(&length_dso, ui_cursor.length_expected);
	if(ui_cursor.length_actual < 10)    ui_cursor.length_actual = 10;
	oled_draw_rounded_rectangle(ui_cursor.x_pos_actual, ui_cursor.y_pos_actual, ui_cursor.width_actual, ui_cursor.length_actual, CURSOR_RECTANGLE_ROUNDED, 0);
	
	/*更新右侧的滑动杆*/
	oled_draw_line(OLED_COLUMN - SLIDER_DISTANCE_RIGHT, 2, OLED_COLUMN - SLIDER_DISTANCE_RIGHT, 62);  /*长60*/
	/*绘制滑动杆上的滑条*/
	if(page_item_num != 0){
	    uint8_t expected_slid_pos = 60 * ((float)ui_cursor.absolute_pos / page_item_num);
		uint8_t actual_slid_pos = dso_system_cal(&slider_dso, expected_slid_pos);
	    oled_draw_rectangle(SLIDER_START_POS_X, actual_slid_pos, SLIDER_WIDTH, SLIDER_WIDTH, 1);
	}

	oled_buffer_apply_mask();
	oled_refresh();
}

/************************************************************************************
* Function: [ui_page_forward] 进入当前光标所指的菜单
*
* Warnings: None
*
* History:    2024-01-14 AHeiDaBai : First version
* History:    2024-01-29 AHeiDaBai : 增加了对小页面(select element)的支持
*==================================================================================*/
void ui_page_forward(void)
{
    if(ui_cursor.obj == NULL || ui_stack.top >= MAX_STACK_LENGTH)    return;
	if(ui_cursor.obj->type != UI_PAGE && ui_cursor.obj->type != UI_SELECT_ELEMENT)
		return;
	
	/*压栈*/
    ui_stack.base[ui_stack.top] = ui_cursor;
    ui_stack.top ++;
	
	ui_cursor.pos = 0;  /*记录在OLED上的位置，主要用于cursor up和cursor down*/
	ui_cursor.y_pos_expected = 0;

	/*不管是正常的页面还是SELECT ELEMENT的小页面，都需要absolute_pos来记录绝对位置*/
	if(ui_cursor.obj->type == UI_PAGE){
		ui_cursor.absolute_pos = 0;
		//ui_cursor.y_pos_actual   = 0;   /*此处不可以将actual_pos归0*/
		/*下一个页面*/
		ui_cursor.page = ui_cursor.obj;  
		/*下个页面的第一个obj*/
		ui_cursor.obj  = ui_cursor.page->priv_charac.page_charac.start; 
		/*实现字符串从左向右滑动（此时ui_cursor.obj指向的一定是第一个obj）*/
		set_obj_name_dso_params(ui_cursor.obj); 
	}else if(ui_cursor.obj->type == UI_SELECT_ELEMENT){
		
		if(ui_cursor.obj->priv_charac.item_charac.val <= 4){
			ui_cursor.pos = ui_cursor.obj->value;
			ui_cursor.y_pos_expected = ui_cursor.pos * 16;
		}
		
		/*光标的绝对位置就是select element当前的值*/
		ui_cursor.absolute_pos = ui_cursor.obj->value;
		ui_cursor.enter_select_element_flag = 1;
		/*进入小页面不需要修改ui_cursor.page和ui_cursor.obj*/
	}
}

/************************************************************************************
* Function: [ui_page_backward] 退回到上一个菜单
*
* Warnings: None
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void ui_page_backward(void)
{
    if(ui_stack.top == 0)    return;
	
	if(ui_cursor.enter_select_element_flag == 0){
	    set_obj_name_dso_params(NULL);
	}else{
		boundaries_expected = OLED_COLUMN - 1;
	}
	
	/*弹栈*/
	ui_stack.top --;
    ui_cursor = ui_stack.base[ui_stack.top];
}

/************************************************************************************
* Function: [ui_cursor_down] OLED屏幕的光标进行下移
*
* Warnings: None
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void ui_cursor_down(void)
{
	if(ui_cursor.obj == NULL)    return;
	
	/*判断是否到达页面底部*/
	if(ui_cursor.enter_select_element_flag != 0){
		if(ui_cursor.obj->value == ui_cursor.obj->priv_charac.item_charac.val - 1)
			return;
	}else{
		if(ui_cursor.obj->next == NULL)
			return;
	}

	/*当光标不在最下方时修改器Y轴期望位置*/
	if(ui_cursor.pos != 3){  /*ui_cursor.pos只能取0-3*/
		ui_cursor.y_pos_expected += 16;
		ui_cursor.pos ++;
	}
	ui_cursor.absolute_pos ++;
	
	/*正常页面操作时需要移动到链表的下一个位置，小页面时用absolute_pos指示位置*/
	if(ui_cursor.enter_select_element_flag == 0){
	    ui_cursor.obj = ui_cursor.obj->next;
	}else{
		ui_cursor.obj->value = ui_cursor.absolute_pos;
	}
}

/************************************************************************************
* Function: [ui_cursor_up] OLED屏幕的光标进行上移
*
* Warnings: None
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void ui_cursor_up(void)
{
	if(ui_cursor.obj == NULL || ui_cursor.obj->prior == NULL)    return;
	if(ui_cursor.absolute_pos == 0)    return;
	
	/*当光标不在最上方时修改其Y轴期望位置*/
	if(ui_cursor.pos != 0){
		ui_cursor.y_pos_expected -= 16;
		ui_cursor.pos --;
	}
	ui_cursor.absolute_pos --;
	
	/*小页面(select element页面)不需要修改ui_cursor.obj*/
	if(ui_cursor.enter_select_element_flag == 0){
	    ui_cursor.obj = ui_cursor.obj->prior;
	}else{
		ui_cursor.obj->value = ui_cursor.absolute_pos;
	}
}

/************************************************************************************
* Function: [ui_cursor_up] UI初始化
*
* Warnings: ！！此函数不可在创建菜单界面函数之前执行！！
*
* History:    2024-01-14 AHeiDaBai : First version
*==================================================================================*/
void ui_settings_init(void)
{
    oled_hardware_init();
	
	/*光标和栈相关初始化*/
    ui_stack.top = 0;
    ui_cursor.pos = 0;
    ui_cursor.page = &first_page;
    ui_cursor.obj = first_page.priv_charac.page_charac.start;
	ui_cursor.y_pos_actual = 0;
	ui_cursor.y_pos_expected = 0;
	ui_cursor.absolute_pos = 0;
	ui_cursor.enter_select_element_flag = 0;
	
	/*离散二阶系统初始化*/
	dso_system_init(&x_pos_dso, POSITION_DSO_SYS_T, POSITION_DSO_SYS_ZETA, POSITION_DSO_SYS_OMEGA);
	dso_system_init(&y_pos_dso, POSITION_DSO_SYS_T, POSITION_DSO_SYS_ZETA, POSITION_DSO_SYS_OMEGA);
	dso_system_init(&width_dso, WIDTH_DSO_SYS_T, WIDTH_DSO_SYS_ZETA, WIDTH_DSO_SYS_OMEGA);
	dso_system_init(&length_dso, WIDTH_DSO_SYS_T, WIDTH_DSO_SYS_ZETA, WIDTH_DSO_SYS_OMEGA);
	dso_system_init(&slider_dso, SLIDER_DSO_SYS_T, SLIDER_DSO_SYS_ZETA, SLIDER_DSO_SYS_OMEGA);
	dso_system_init(&mask_dso, SLIDER_DSO_SYS_T, SLIDER_DSO_SYS_ZETA, SLIDER_DSO_SYS_OMEGA);
	mask_dso.last_input = mask_dso.last_last_input = mask_dso.last_output = mask_dso.last_last_output = OLED_COLUMN - 1;
	for(uint8_t i = 0; i < OBJ_NAME_NUMBER; ++i){
		dso_system_init(&name_dso[i], NAME_DSO_SYS_T, NAME_DSO_SYS_ZETA, NAME_DSO_SYS_OMEGA);
	}
	set_obj_name_dso_params(ui_cursor.obj);  /*ui_cursor.obj指向的一定是第一个obj*/
}
