#ifndef _FLUENT_UI_DISCRETE_SECOND_ORDER_SYSTEM
#define _FLUENT_UI_DISCRETE_SECOND_ORDER_SYSTEM

typedef struct dso_system_s{
	float a0, a1, a2;
	float b0, b1, b2;
	
	float last_input, last_last_input;
	float last_output, last_last_output;
}dso_system_t;

float dso_system_cal(dso_system_t *sys, float input);
void  dso_system_init(dso_system_t *sys, float T, float zeta, float omega);

#endif /*_FLUENT_UI_DISCRETE_SECOND_ORDER_SYSTEM*/
