# Fluent-UI  (适用于0.96寸OLED的丝滑UI框架)
## 为什么会有这个项目？ :rocket:

我自己在做嵌入式竞赛和项目的时候感觉板子不能实时显示一些状态等信息真的很痛苦，虽然会有一些串口工具等辅助调试，但是串口工具依赖于电脑，总是一边看着板子，一边看着电脑，板子和电脑还不能离得太远，这就体现出OLED屏幕的方便，但是嵌入式UI框架并没有电脑端那么丰富，而且单片机资源比较紧张，这就造成了大部分的OLED的UI很丑，于是趁自己空闲的时候写了这个项目。

这个UI框架诞生的目的就是为了方便观测状态，因此仅仅限于0.96寸OLED屏幕，对于大型的用于用户交互的屏幕，LVGL等是更好的选择。但是此项目不局限于STM32，可以拓展到更多的单片机。

在写此UI框架的时候参考了一些资料，有些想法也是看了别人的视频才想到的，这些都放在了项目最后。

## 如何移植此项目到其他单片机？ :rocket:

**本项目目前只提供了STM32F103VET6（HAL库）的接口，本项目中的硬件抽象集成到了oled_portable.h中，其他类型的单片机想要使用本项目只需更改oled_portable.h中的接口即可**

注：博主所用的0.96寸OLED屏幕的主控为SSD1306，其他主控与SSD1306的不同在于控制指令（大部分控制指令是相同的），如果使用其他主控时发现了一些异常，可以参考商家给的例程修改oled_driver.c的`oled_hardware_init`函数中的内容。移植时最好先验证刷新屏幕的spi函数可用。

## 如何使用这个框架？ :rocket:
- **STEP1.** 此UI框架共有7个文件（.c和.h文件，放在Fluent_UI文件夹下），首先你需要将这7个文件添加到自己的项目中去。
- **STEP2.** 在合适的.c文件中添加`#include "fluent_ui.h"`，然后需要根据自己的需要使用API构建起自己的UI界面。
- **STEP3.** 在构建完UI界面后，调用`ui_settings_init()`函数进行设定的初始化（**注意ui_settings_init函数不可在构建完UI界面前调用**）。
- **STEP4.** 利用硬件定时器或者软件定时器等定期调用`ui_refresh()`函数（**注意`ui_refresh()`函数的调用周期==必须==与fluent_ui_config.h文件中的OLED_REFRESH_CYCLE宏一致**）。
- **STEP5.** 根据自己的需要调用各种API，如使光标上下移动的函数(`ui_cursor_up()`、`ui_cursor_down()`)、进入和退出页面的函数(`ui_page_forward()`、`ui_page_backward()`)。

**注意：`fluent_ui_config.h`中`MAX_STACK_LENGTH`宏指示了菜单的最大深度，在自定义菜单时，需要根据菜单的深度来修改`MAX_STACK_LENGTH`宏。**


例程说明和提供的API说明请参考这个文档：[**Fluent-UI API说明**](./Fluent_UI/API_DOC.md)

同时本项目也提供了`fluent_ui_config.h`文件，此文件中提供了与UI相关的一些设置，大部分保持默认即可。

**建议屏幕的刷新速度（即ui_refresh()函数的执行周期）在50ms左右（大约20帧/秒刷新率）即可，人眼不会感到明显的卡顿**

## 关于例程 :rocket:

Example文件夹下提供了使用此UI框架的例程，使用的单片机为：STM32F103VET6（72M主频），STM32CubeMX+HAL库，使用了硬件SPI（SPI1, 无DMA），具体的硬件配置可以使用STM32CubeMX打开OLED_DRIVER.ioc文件进行查看。例程中部分思路在[<Fluent-UI API文档">](./Fluent_UI/API_DOC.md)中也有说明。

经粗略测量（ST-LINK + Keil软件测量），在STM32F103VET6（72MHz） + SPI（无DMA）的硬件条件和编译器-O1优化下，屏幕刷新主函数`oled_refresh()`的执行速度大概在1.4ms左右，如果使用DMA的话应该还能更快一些。

| 硬件平台                                  |   编译器优化等级   |  执行时间  |
| :---------------------------------------- | :----------------: | :--------: |
| STM32F103VET6（72MHz HAL） + 硬件SPI（无DMA） | 不进行优化（即O0） | 2.46ms左右 |
| STM32F103VET6（72MHz HAL） + 硬件SPI（无DMA） |     O1级别优化     | 1.4ms左右  |

>  注：不同的UI菜单其运行时间必然不同，此处所指的运行速度是指在例程的UI菜单下第一页的运行时间。

## 关于运动动画效果 :rocket:

动画使用了二阶离散系统，通过调整阻尼比和自然震荡频率来调整动画的效果，具体的原理参见这篇文章：

[使用《自控原理》制作丝滑运动动画 - 拉普拉斯的狗的文章 - 知乎](https://zhuanlan.zhihu.com/p/650188735)

Matlab文件夹下提供的是此文章的代码。


## 致谢 :rocket:
1. :four_leaf_clover: 这个项目的启发是**稚晖君**的一个UI视频，B站视频：【最近为了搞主线机器人在补一些自控原理的知识，突发奇想发现传递函数可以用来绘制动画用！把动画曲线等价为一个二阶弹簧阻尼系统，通过调整零极点参数可以方便地呈现出各…】 https://www.bilibili.com/video/BV1Vg411B7kw/?share_source=copy_web&vd_source=2968b57cc30a319997ed4862c9fcae40
2. :four_leaf_clover: 这个项目的UI布局等也参考了这个视频：【稚晖君 MonoUI: A Very Smooth OLED UI Framework】 https://www.bilibili.com/video/BV1a3411g7NS/?share_source=copy_web&vd_source=2968b57cc30a319997ed4862c9fcae40. 视频中UI框架的作者也是**稚晖君**。
3. :four_leaf_clover: 大佬的视频（【如何用数学让动画变得极致丝滑？【中英双字】Giving Personality to Procedural Animations using Math】 https://www.bilibili.com/video/BV1wN4y1578b/?share_source=copy_web&vd_source=2968b57cc30a319997ed4862c9fcae40） 给了我一些动画思路，但我最终的动画实现方式和其有些不同。
4. :four_leaf_clover: 这个项目的一些OLED驱动函数来源于**中景园电子**的思路。
5. :four_leaf_clover: 项目中的例程中关于按键的文件（multi_key.c、multi_key.h）是由**SeekFree**的开源库中的文件修改和移植的，其开源链接在：https://gitee.com/seekfree/TC264_Library/blob/master/Seekfree_TC264_Opensource_Library/libraries/zf_device/zf_device_key.c
6. :four_leaf_clover:  项目中的`oled_draw_rounded_rectangle`函数部分代码来源自B站up主**大明狐**，视频链接：https://www.bilibili.com/video/BV1pG411C7xP/?share_source=copy_web
7. :four_leaf_clover: keil代码运行时间测量方法：[使用Keil精确测试代码运行时间 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/346362143)
8. :four_leaf_clover: 参考内容整理过程中难免出现遗漏，如果您发现此项目存在您的相关内容但并没有被提及，请及时和我联系。
