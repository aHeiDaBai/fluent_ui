/*********************************************************************************************************************
* TC264 Opensourec Library 即（TC264 开源库）是一个基于官方 SDK 接口的第三方开源库
* Copyright (c) 2022 SEEKFREE 逐飞科技
*
* 本文件是 TC264 开源库的一部分
*
* TC264 开源库 是免费软件
* 您可以根据自由软件基金会发布的 GPL（GNU General Public License，即 GNU通用公共许可证）的条款
* 即 GPL 的第3版（即 GPL3.0）或（您选择的）任何后来的版本，重新发布和/或修改它
*
* 本开源库的发布是希望它能发挥作用，但并未对其作任何的保证
* 甚至没有隐含的适销性或适合特定用途的保证
* 更多细节请参见 GPL
*
* 您应该在收到本开源库的同时收到一份 GPL 的副本
* 如果没有，请参阅<https://www.gnu.org/licenses/>
*
* 额外注明：
* 本开源库使用 GPL3.0 开源许可证协议 以上许可申明为译文版本
* 许可申明英文版在 libraries/doc 文件夹下的 GPL3_permission_statement.txt 文件中
* 许可证副本在 libraries 文件夹下 即该文件夹下的 LICENSE 文件
* 欢迎各位使用并传播本程序 但修改内容时必须保留逐飞科技的版权声明（即本声明）
*
* 文件名称          zf_device_key
* 公司名称          成都逐飞科技有限公司
* 版本信息          查看 libraries/doc 文件夹内 version 文件 版本说明
* 开发环境          ADS v1.9.20
* 适用平台          TC264D
* 店铺链接          https://seekfree.taobao.com/
*
* 修改记录
* 日期              作者                备注
* 2022-09-15       pudding            first version
* 2023-04-28       pudding            增加中文注释说明
********************************************************************************************************************/

#include "multi_key.h"
#include "tim.h"
#include "my_ui.h"

static GPIO_TypeDef*   key_port[KEY_NUMBER] = KEY_PORT_LIST;
static const uint16_t  key_pin[KEY_NUMBER] = KEY_PIN_LIST;

static uint32_t               scanner_period = 1;                                 // 按键的扫描周期ms
static uint32_t               key_press_time[KEY_NUMBER];                         // 按键信号持续时长
static key_state_enum         key_state[KEY_NUMBER];                              // 按键状态

void key_scanner (void)
{
    for(uint8_t i = 0; KEY_NUMBER > i; i ++){
        if(KEY_RELEASE_LEVEL != HAL_GPIO_ReadPin(key_port[i], key_pin[i])){                   // 按键按下
            key_press_time[i] ++;
			/*保证了KEY_LONG_PRESS出现一次后消失*/
			if(key_state[i] == KEY_LONG_PRESS){
				key_state[i] = KEY_RELEASE;
				key_press_time[i] = 0;
				continue;
			}
			
            if(KEY_LONG_PRESS_PERIOD / scanner_period <= key_press_time[i]){
                key_state[i] = KEY_LONG_PRESS;
            }
        }else{                                                                    // 按键释放
            if((KEY_LONG_PRESS != key_state[i]) && (KEY_MAX_SHOCK_PERIOD / scanner_period <= key_press_time[i])){
                key_state[i] = KEY_SHORT_PRESS;
            }else{
                key_state[i] = KEY_RELEASE;
            }
            key_press_time[i] = 0;
        }
    }
}

key_state_enum key_get_state (key_index_enum key_n)
{
    return key_state[key_n];
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	 if(htim == &htim6){
	     key_scanner();
		 if(key_state[KEY_1] == KEY_SHORT_PRESS){
			 ui_cursor_down();
		 }else if(key_state[KEY_1] == KEY_LONG_PRESS){
			 ui_page_forward();
		 }
		 
		 if(key_state[KEY_2] == KEY_SHORT_PRESS){
			 ui_cursor_up();
		 }else if(key_state[KEY_2] == KEY_LONG_PRESS){
			 ui_page_backward();
		 }
		 
		 if(key_state[KEY_3] == KEY_SHORT_PRESS){
			 ui_add();
		 }
		 
		 if(key_state[KEY_4] == KEY_SHORT_PRESS){
			 ui_sub();
		 }
	 }
}


