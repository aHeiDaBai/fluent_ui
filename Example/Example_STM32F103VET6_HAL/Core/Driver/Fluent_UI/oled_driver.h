#ifndef _FLUENT_UI_OLED_DRIVER_H
#define _FLUENT_UI_OLED_DRIVER_H

#include "oled_portable.h"

#define OLED_ROW       8
#define OLED_COLUMN    128

uint8_t oled_send_cmd(uint8_t cmd);
void oled_refresh(void);
void oled_clear(void);
void oled_clear_buffer(void);
void clear_square_buffer(uint8_t x0, uint8_t y0, uint8_t length, uint8_t width);
void clear_right_part_buffer(uint8_t boundaries);
void oled_clear_mask(void);
void oled_show_mask(void);
void oled_buffer_apply_mask(void);
void oled_draw_point(uint8_t x, uint8_t y);
void oled_draw_line(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);
void oled_draw_rectangle(uint8_t x0, uint8_t y0, uint8_t length, uint8_t width, uint8_t buffer_select);
void oled_draw_rounded_rectangle(uint8_t x0, uint8_t y0, uint8_t length, uint8_t width, float round, uint8_t buffer_select);
void oled_write_char_F6X8(uint8_t x, uint8_t y, char data);
void oled_write_char_F8X16(uint8_t x, uint8_t y, char data);
void oled_write_string_F6X8(int16_t x, uint8_t y, char * str);
void oled_write_string_F8X16(int16_t x, uint8_t y, char * str);
void oled_hardware_init(void);
	
#endif /*_FLUENT_UI_OLED_DRIVER_H*/
