#ifndef _FLUENT_UI_OLED_PORTABLE_H
#define _FLUENT_UI_OLED_PORTABLE_H

#include "spi.h"
#include "main.h"

/*提供延时函数*/
#define fluent_ui_delay_ms  HAL_Delay

/*提供发送数据的函数*/
/*data为数据数组的首地址，len为要发送的数据长度，time为超时等待时间*/
#define oled_send_buffer(data, len, time)    HAL_SPI_Transmit(&hspi1, (data), (len), (time))

/*提供将DC置为1或置为0的函数*/
#define set_dc_state()         HAL_GPIO_WritePin(DC_GPIO_Port, DC_Pin, GPIO_PIN_SET)
#define reset_dc_state()       HAL_GPIO_WritePin(DC_GPIO_Port, DC_Pin, GPIO_PIN_RESET)

/*提供将CS置为1或置为0的函数*/
#define set_cs_state()         HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET)
#define reset_cs_state()       HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET)

/*提供将RES置为1或置为0的函数*/
#define set_res_state()        HAL_GPIO_WritePin(RES_GPIO_Port, RES_Pin, GPIO_PIN_SET)
#define reset_res_state()      HAL_GPIO_WritePin(RES_GPIO_Port, RES_Pin, GPIO_PIN_RESET)

#endif /*_FLUENT_UI_OLED_PORTABLE_H*/
