#ifndef _FLUENT_UI_MY_UI_H
#define _FLUENT_UI_MY_UI_H

#include "oled_portable.h"

typedef enum ui_type_e{
    UI_PAGE            = 0,
    UI_CHECK_BOX_ITEM  = 1,
	UI_FLOAT_ITEM      = 2,
	UI_INT8_ITEM       = 3,
	UI_INT16_ITEM      = 4,
	UI_INT32_ITEM      = 5,
	UI_UINT8_ITEM      = 6,
	UI_UINT16_ITEM     = 8,
	UI_UINT32_ITEM     = 9,
	UI_SELECT_ELEMENT  = 10,
	UI_STRING          = 11,
	UI_ERROR,
}ui_type_t;

typedef struct ui_obj_s
{
	/*基础属性*/
	uint8_t          type;    //page? item?
	uint8_t          id;
	uint8_t          name_len;
	uint8_t          value;  /*小页面时记录当前选择的值*/
	
	uint16_t         identity_id;  /*identity_id只有在obj为item时才会有效*/
	uint8_t          max_len; /*为UI_SELECT_ELEMENT时才有效*/
    struct ui_obj_s *prior;   //指向前一个obj
    struct ui_obj_s *next;    //指向下一个obj
    char            *name;

	union{
		/*page独有属性*/
		struct{
	        uint8_t          item_num; //该页的元素总个数
            struct ui_obj_s *start;  //如果此obj为page，则start指向此obj的第一个obj
		}page_charac;
		/*item独有属性*/
		struct{
			uint8_t val;
			void    *data;
	        void   (*draw_element)(struct ui_obj_s *self, uint8_t pos);
		}item_charac;
	}priv_charac;
}ui_obj_t;

extern ui_obj_t first_page;

/*添加page和item*/
void ui_add_page(ui_obj_t *page, char *name, ui_obj_t *last_page, uint8_t id);
void ui_add_item(ui_obj_t *item, char *name, ui_obj_t *last_page, uint8_t id, uint8_t type, void *data, uint8_t precision);

/*获取光标所指的obj的identity_id*/
uint16_t  ui_get_cursor_identity_id(void);
/*获取光标所指的obj的type*/
ui_type_t ui_get_cursor_obj_type(void);
/*增加光标所指的obj的value（注意此函数不会检查是否溢出）*/
void ui_inc_cursor_obj_val(float inc_val);

/*check box 相关函数*/
uint8_t ui_get_check_box_value(ui_obj_t *item);
void ui_set_check_box_value(ui_obj_t *item, uint8_t new_val);

/*进入页面，退出页面，光标上下移动的相关函数*/
void ui_page_forward(void);
void ui_page_backward(void);
void ui_cursor_down(void);
void ui_cursor_up(void);

/*UI界面的刷新*/
void ui_refresh(void);

/*UI设定初始化，在UI界面设定完后才可以调用ui_settings_init*/
void ui_settings_init(void);

#endif /*_FLUENT_UI_MY_UI_H*/
