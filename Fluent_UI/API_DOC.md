# Fluent-UI  Document  

## 说明

本文档是对如何使用Fluent_UI的进一步说明，也是对Fluent-UI中API用途的说明。

本文档提供了PDF版本和Markdown版本，推荐阅读Markdown版本

==**下面函数的使用均需要引用头文件`fluent_ui.h`，即：`#include "fluent_ui.h"`**==

**前置知识：在Fluent-UI框架中，大致有两类(obj)，一类是页面(page)，另一类是页面中的元素(item)，其中item也分为更多的类别，具体可见fluent_ui.h中的ui_type_t枚举。每一个obj，都只能挂载到一个页面下。**

## 目录
[toc]

---------------------------

## 1.光标移动和画面切换函数 :rocket:
### 向下移动光标
调用函数`ui_cursor_down()`可将光标移动向下。
光标的移动函数相当于只是发送了一个光标向下移动的讯息，真正实现画面中光标移动的函数是`ui_refresh()`

<p align = "center">    
<img src="../PIC/cursor_down.gif" style="zoom:40%;" >
</p>

### 向上移动光标

调用函数`ui_cursor_up()`可将光标移动向上
光标的移动函数相当于只是发送了一个光标向上移动的讯息，真正实现画面中光标移动的函数是`ui_refresh()`

<p align = "center">    
<img src="../PIC/cursor_up.gif" style="zoom:40%;" >
</p>

### <span id="page_forward"> 进入下一个页面 </span>

调用`ui_page_forward()`函数可以切换到当前光标指向的下一个页面(只有当前光标指向的对象(obj)是"页"(page)时，`ui_page_forward()`函数才会起作用)。
画面的切换函数相当于只是发送了一个切换展示的讯息，真正实现画面切换的函数是`ui_refresh()`

**注意：如果一个页面是空页面，也就是此页面中没有挂载任何的page或item，那么`ui_page_forward()`函数是不起作用的**

<p align = "center">    
<img src="../PIC/ui_forward.gif" style="zoom:40%;" >
</p>

### 回退到上一个页面

调用`ui_page_backward()`函数可以切换到上一个页面。
画面的切换函数相当于只是发送了一个切换展示的讯息，真正实现画面切换的函数是`ui_refresh()`

<p align = "center">    
<img src="../PIC/ui_backward.gif" style="zoom:40%;" >
</p>

---------------
## <span id="build_menu">2. 构建自己的菜单 :rocket:</span>

**构建菜单只需用到两个API：`ui_add_page`函数或`ui_add_item`函数**

使用`ui_add_page`函数或`ui_add_item`函数可以将page或item添加到某一页，在Fluent-UI中，有一个特殊的page，就是“第一页（first page）”，添加到第一页的obj（包括page或item），在开发板上电OLED屏幕初始化完成后就可以看到，添加到其他页的obj必须在进入对应页面后才可以看到。first_page在fluent_ui.c中已经定义好了，可以直接引用。

### 如何将一个page挂载到另外的页面？
STEP1. 利用ui_obj_t创建一个页面
STEP2. 使用`ui_add_page`函数将页面名称添加到某个页面中。

函数原型为：
```c
void ui_add_page(ui_obj_t *page, char *name, ui_obj_t *last_page, uint8_t id);
```
具体的参数说明为：
[page] 要添加的page结构体的地址
[name] 要添加的page的名字，用于在界面上进行展示
[last_page, id] 说明了要展示在哪个页面的哪个位置

例如：现在想要在首页添加一个PID的页面，在页面上展示的内容为"+ PID"(此处的+加一个空格只是为了区分此为页面，也可以将其换成其他内容), 添加此PID的页面后，就可以使用光标移动函数调整光标到PID处，然后使用页面进入函数（即`ui_cursor_up()`）进入此页面。
那么添加页面的代码可以这样写：

```c
ui_obj_t PID_page;  /*注意要定义成全局变量，或被static修饰的函数内变量*/
......
ui_add_page(&PID_page, "+ PID", &first_page, 2);
```
函数参数中的&first_page表示将PID_page添加到首页，'2'只是表示了PID_page在first_page中的展示顺序。
效果如下图的第三行所示，此时如果将光标移动到"+ PID"处，然后使用`ui_page_forward()`函数就可以进入到"+ PID"页面中。

<p align = "center">    
<img src="../PIC/PID.jpg" style="zoom:20%;" >
</p>

--------

### 如何在页面中添加item?

添加item需要用到`ui_add_item`函数，添加不同类型item的区别在于`ui_add_item`函数中的`type`参数不同, `ui_add_item`函数原型为：
```c
void ui_add_item(ui_obj_t *item, char *name, 
                 ui_obj_t *last_page, uint8_t id, 
                 uint8_t type, void *data, uint8_t precision);
```
参数说明：
> [item] : 要添加的item结构体的地址
> [name] : 要添加的item的名字，用于在界面上进行展示
> [last_page, id] : 说明了要展示在哪个页面的哪个位置
> [type] : 决定了此item的类型。从ui_type_t中选择。
> [data] : 要挂载的数据的地址(如果是UI_CHECK_BOX_ITEM类型，则此参数可以任填，如NULL)
> [precision] : 展示多少位小数（只对UI_FLOAT_ITEM浮点类型有效，其余类型可任填，如0）

#### （1）<span id="add_check_box">:four_leaf_clover:如何添加一个check box</span>
创建一个check box需要两步。
> STEP1.  创建一个ui_obj_t对象。
> STEP2.  调用`ui_add_item`函数。

假设现在需要屏幕上显示一个表示电机输出的check box，当此check box被置为1时，电机进行输出，当此check box被置为0时，电机不进行输出。

第一步，创建一个ui_obj_t对象，即：`ui_obj_t motor_out_check_box;`（注意这个对象要么是全局变量，要么是被static修饰的函数内变量，由于更改check box的值需要用到此对象，因此这里定义成全局变量）。

第二步，调用`ui_add_item`函数，假设我们希望这个check box在屏幕上显示的内容是"- Motor"(这里在Motor前面加上一个-和空格只是为了表示此内容不是page，这里完全可以写成"Motor")，并且希望其挂载到page1这个页面下（page1是一个ui_obj_t类型的变量，并且其是页(page)），可以这样写：
```c
ui_add_item(&motor_out_check_box, "- Motor", &first_page, 3, UI_CHECK_BOX_ITEM, NULL, 0);
```
参数中'3'决定了"- Motor"在展示页面时的位置（页面中内容展示的先后顺序由此参数决定，从小到大的顺序对应了从屏幕上方到屏幕下方），参数中UI_CHECK_BOX_ITEM表示此item是一个check box，参数中的NULL和0在创建check box时不起作用，这两个参数可以给任意值。
这样就完成了check box的创建，其效果如下图所示（左图是check box未被光标选中，右图是check box被光标选中）。
<p align = "center">    
<img src="../PIC/check_box.png" style="zoom:100%;" >
</p>

#### （2）<span id="add_num">:four_leaf_clover:如何添加数值的显示</span>

Fluent-UI框架可以展示特定的变量的值，共有七种。

|     参数宏     |        对应的类型        |
| :------------: | :---------------------: |
| UI_FLOAT_ITEM  |    浮点数，对应float     |
|  UI_INT8_ITEM  |  有符号8位，对应int8_t   |
| UI_INT16_ITEM  | 有符号16位，对应int16_t  |
| UI_INT32_ITEM  | 有符号32位，对应int32_t  |
| UI_UINT8_ITEM  |  无符号8位，对应uint8_t  |
| UI_UINT16_ITEM | 无符号16位，对应uint16_t |
| UI_UINT32_ITEM | 无符号32位，对应uint32_t |

例如现在想要显示里程，其为无符号十六位，其在屏幕上显示的内容为"- Meter"，并将其挂载到第一页，其可以这样写：

```c
ui_obj_t meter_item; /*注意要定义成全局变量，或被static修饰的函数内变量*/
uint16_t meter = 5600;
......
ui_add_item(&meter_item, "- Meter", &first_page, 3, UI_UINT16_ITEM, &meter, 0);
```
上面`ui_add_item`函数中参数`&meter`表示将meter_item与meter变量进行绑定，当meter变量出现变化时，屏幕上对应的内容也会改变，`ui_add_item`函数中最后一个参数'0'无意义，可以填任何数。上面程序的显示效果如下图（第四行）所示：
<p align = "center">    
<img src="../PIC/meter.png" style="zoom:100%;" >
</p>

由于其挂载到了第一页，故当开发板上电后就可以看到Meter变量。

假设现在想要显示浮点数，想要展示当前的速度，其在屏幕上显示的内容为"- Speed"，并且想要其小数部分只展示到第三位，并将其挂载到第一页，其可以这样写：

```c
ui_obj_t speed_item; /*注意要定义成全局变量，或被static修饰的函数内变量*/
float speed = 3.4789;/*注意要定义成全局变量，或被static修饰的函数内变量*/
......
ui_add_item(&speed_item, "- Speed", &first_page, 0, UI_FLOAT_ITEM, &speed, 3);
```
显示效果如下图的第一行：
<p align = "center">    
<img src="../PIC/meter.png" style="zoom:100%;" >
</p>
`ui_add_item`函数中的参数3表示只显示到小数点后三位，可见speed变量的值为3.4789，但是屏幕上只显示了3.478。

#### （3）<span id="add_str">:four_leaf_clover:如何添加字符串的显示</span>
`UI_STRING`类型的作用是展示某个字符数组，item会记住此字符数组的地址，所以**在此字符数组上做的改动都会在屏幕上直接显示。**

```c
/*定义需要挂载的字符数组并进行初始化*/
static ui_obj_t PID_type_item;
static char PID_type_buffer[10];
memset(PID_type_buffer, 0, sizeof(PID_type_buffer);
/*初始化显示"*PID*"*/
strcpy(PID_type_buffer, "*PID*");
/*将字符数组进行挂载*/
ui_add_item(&PID_type_item, "- Type", &pid_page, 0, UI_STRING, PID_type_buffer, 0);
......
/*现在想要此字符数组展示"*PI*"*/
strcpy(PID_type_buffer, "*PI*");
/*修改了此字符数组，屏幕上就会对应进行变化*/
```
当添加字符串类型的显示时，`ui_add_item`函数中的最后一个参数是不起作用的，函数中的`data`参数是字符数组的地址。
显示效果如下：

<p align = "center">    
<img src="../PIC/string.png" style="zoom:100%;" >
</p>

#### （4）:four_leaf_clover:注意事项

每一个item都只能挂载到一个页面下，比如我先使用了`ui_add_item`函数将item1挂载到了first_page下面，那么我就不可以再次使用`ui_add_item`函数将item1挂载到其他页面下面，page同理。

#### （5）:four_leaf_clover:ui_add_item函数汇总

`ui_add_item`参数较多，因此在这里对其进行一个汇总。

函数原型：

```c
void ui_add_item(ui_obj_t *item, char *name, ui_obj_t *last_page, uint8_t id, ui_type_t type, void *data, uint8_t precision)
```

其中item就是创建的ui_obj_t对象的地址，name就是在页面上展示的字符串，last_page就是要将其挂载到哪个页面，id指示了其在页面中的相对位置（id越小，其在页面中的位置越靠上），type指明了展示的类型。

挂载不同类型值的时候data参数和precision参数的意义：

|               | type              | data           | precision      |
| ------------- | ----------------- | -------------- | -------------- |
| 展示浮点数    | UI_FLOAT_ITEM     | 浮点变量的地址 | 展示多少位小数 |
| 展示整形      | UI_UINT8_ITEM等   | 整形变量的地址 | 无意义         |
| 展示check box | UI_CHECK_BOX_ITEM | 无意义         | 无意义         |
| 展示字符串    | UI_STRING         | 字符数组的地址 | 无意义         |


-------------------


## <span id="get_set_check_box">3. check box相关：获取和设置check box的值 :rocket:</span>
### （1）获取check box的值

使用`ui_get_check_box_value`函数可以获取到check box的值，其原型为：
```c
uint8_t ui_get_check_box_value(ui_obj_t *item);
```
下图展示了check box的两种状态：
左图表示check box未被选中，`ui_get_check_box_value`函数返回0，
右图表示check box被选中，`ui_get_check_box_value`函数返回1。

<p align = "center">    
<img src="../PIC/check_box_2states.png" style="zoom:100%;" >
</p>
依旧是上面的电机例子，此时想获取电机check box的值，可以这样写：

```c
uint8_t check_box_state = ui_get_check_box_value(&motor_out_check_box);
```

则上图中左图会返回0，上图中右图会返回1

### （2）设定check box的值

使用`ui_set_check_box_value`函数可以设定check box的值，其函数原型为：

```c
void ui_set_check_box_value(ui_obj_t *item, uint8_t new_val);
```
依旧是上面的电机例子，此时想将电机check box的值设为1，可以这样写：

```c
ui_set_check_box_value(&motor_out_check_box, 1);
```

-------------

## 4.如何知晓光标指向的是哪个item? :rocket:

假设现在有这样一个场景：我们在屏幕上展示了变量Kp，当光标指向Kp时，我们按下按键1（KEY1）一次，对变量Kp进行加0.1。
首先这个问题的思路应该是当KEY1按下后，判断当前光标指向的是否是Kp，如果指向是Kp，那么对其进行加0.1操作，否则直接退出。判断KEY1按下较简单（外部中断或ms级别的高低电平扫描），关键问题是如何判断当前光标指向的是Kp。

当调用`ui_add_item`函数后，Fluent-UI会为每一个item分配一个独一无二的identity_id，此identity_id就存储在item的结构体的identity_id成员中，可以通过identity_id来判断当前光标指向的item。
Fluent-UI提供了一个API函数可以获取当前光标指向的item的identity_id，为`ui_get_cursor_identity_id`，其函数原型为：

```c
uint16_t ui_get_cursor_identity_id(void)
```
说回到上面的问题，利用`ui_get_cursor_identity_id`函数，整个问题的伪代码如下：
```c
ui_obj_t Kp_item;  /*注意要定义成全局变量，或被static修饰的函数内变量*/
static float Kp = 1.324;  /*注意要定义成全局变量，或被static修饰的函数内变量*/
......
ui_add_item(&Kp_item, "- Kp", &first_page, 2, UI_FLOAT_ITEM, &speed, 3);
......
if(key1 was pressed){ /*按键被按下*/
    if(ui_get_cursor_identity_id() == Kp_item.identity_id){ /*判断光标确实指向了Kp*/
        Kp += 0.1;
    }
}
```

当想要改变的数值比较少时此方法可行，但是一旦当要改变数值的变量多起来会导致if过于冗余，例如我们想要通过一个按键来修改5个变量的数值，那么就会有一个if、四个else if，为了方便，Fluent-UI提供了`ui_get_cursor_obj_type`函数和`ui_inc_cursor_obj_val`函数用于统一修改一类数值的大小。`ui_get_cursor_obj_type`函数可以获取当前光标指向的obj的类型，如UI_FLOAT_ITEM类型等，`ui_inc_cursor_obj_val`函数可以添加或减少当前光标指向的obj的数值，如给当前光标指向的变量加上0.1或者减去0.1。

**注意！ 为了防止此函数的滥用，Fluent-UI限制ui_inc_cursor_obj_val函数==只==可以修改以“#”开头的菜单名称的obj指向的数值**，如下图所示，其中的Speed、Acee、Meter不会受到`ui_inc_cursor_obj_val`函数的影响，因为他们的展示名称不以“#”开头，`ui_inc_cursor_obj_val`函数对他们不起作用，但是`ui_inc_cursor_obj_val`函数可以对Kp、Ki、Kd起作用，因为他们的展示名称以“#”开头，`ui_inc_cursor_obj_val`函数自然不会对"+PID"产生影响，因为他是页面。

<p align = "center">    
<img src="../PIC/#.png" style="zoom:100%;" >
</p>

`ui_get_cursor_obj_type`函数原型为：

```c
ui_type_t ui_get_cursor_obj_type(void)
```

`ui_inc_cursor_obj_val`函数原型为：

```c
void ui_inc_cursor_obj_val(float inc_val)
```

其中inc_val表示要对当前变量做出的改变，如：当前光标指向的变量的数值为0.2，现在调用`ui_inc_cursor_obj_val(-0.1);`，则此变量变为0.1，如果调用`ui_inc_cursor_obj_val(0.1);`则此变量变为0.3。

现在我们想要做到的效果是如果当前光标指向的是浮点型（即UI_FLOAT_ITEM），那么我们每按下按键，就将此变量加0.1，如果当前光标指向的是UINT32（即UI_UINT32_ITEM），那么我们每按下按键，就将此变量加1（当然上述的变量的展示名称均以“#”开头），则伪代码可以像下面这样写：

```c
if(key was pressed){ /*按键被按下*/
    ui_type_t cursor_type = ui_get_cursor_obj_type();  /*获取当前光标指向的obj的类型*/
     switch (cursor_type){
         case UI_FLOAT_ITEM:
             ui_inc_cursor_obj_val(0.1);  /*FLOAT类型的加0.1（需要#开头）*/
             break;

         case UI_INT32_ITEM:
             ui_inc_cursor_obj_val(1);  /*UINT32类型的加1（需要#开头）*/
             break;
             
         default:
             break;
     }
}
```

需要注意一个细节问题：`ui_inc_cursor_obj_val`函数不会对数值溢出进行检查，例如我们想要一个int8类型的进行数值的加减，因为int8类型的范围只在-128~127，因为如果此变量为127，又使用`ui_inc_cursor_obj_val`函数对其进行加法操作，会产生数值的溢出，产生的结果是未定义的，但好在`ui_inc_cursor_obj_val`函数大部分情况下都是用于人机交互，所以只要使用者注意此细节就没有太大的问题，或者也可以直接将变量的类型扩大，全部使用INT32或UINT32进行交互。

## 5. 浮点精度丢失问题（:rocket:重要:rocket:）

任何使用二进制的编程语言都会有浮点精度问题（不清楚浮点数精度问题的可以自行百度或者调试一下看内存中的实际数值），在展示浮点数时，由于浮点数在计算机中的存储问题，如果用户展示的浮点数后面的位数比较小，可能会产生误导，为此，**==强烈建议不要展示低于2位精度的浮点数，强烈建议在展示浮点数时尽可能多的展示几位小数==**，如想要展示Kp，可能一位小数足够，但强烈建议展示到2位或3位小数，为展示浮点精度问题的严重，在example中有一个precision子菜单，其中有三个选项，展示的都是`precision`变量，但其展示的精度不同，如下图所示：

<p align = "center">    
<img src="../PIC/presicion.png" style="zoom:20%;" >
</p>
当对`precision`变量都使用`ui_inc_cursor_obj_val`函数进行减0.1操作时，比较正常，如下图所示：

<p align = "center">    
<img src="../PIC/presicion0.png" style="zoom:20%;" >
</p>

但是当再次对其使用`ui_inc_cursor_obj_val`函数进行减0.1操作时，三个结果出现了不同，如下图所示：

<p align = "center">    
<img src="../PIC/presicion0-9.png" style="zoom:20%;" >
</p>
理想中的结果应该是-0.1，但是由于浮点数精度问题，单片机中实际存储的是-0.09999999......，此时如果展示的精度比较低，如只展示1位小数，会让人误认为此变量的数值为0，但实际上此变量是-0.1，这与显示浮点的函数无关。例如下图，float_dat2理论上的值是912389.9341098，但单片机中存储的实际上是912389.938.....

<p align = "center">    
<img src="../PIC/float.png" style="zoom:35%;" >
</p>


因此强烈建议在展示浮点数时多展示几位小数。

## 6.常见错误:rocket:

1. **将item或page多次进行挂载**，如先将pid_item挂载到了first_page下面，后面又将其挂载到了pid_page下面，这样会造成UI界面的错乱。

```c
static ui_obj_t pid_item;
......
ui_add_item(&pid_item, "- Type",  &first_page, 0, UI_STRING,  PID_type_buffer, 0);
......
ui_add_item(&pid_item, "- Type",  &pid_page, 0, UI_STRING,  PID_type_buffer, 0); /*错误*/
/*不可对同一个obj进行多次挂载*/
```

2. **在同一个页面下挂载id相同的obj**，即在使用`ui_add_item`或`ui_add_page`时出现id的相同，id用于指示其在页面中的顺序，在一个页面下如果出现了相同的id，其结果未定义，如：

```c
static ui_obj_t pid_item, pi_item;
...
ui_add_item(&pid_item, "- PID", &first_page, 0, UI_STRING, PID_type_buffer, 0);
ui_add_item(&pi_item,  "- PI",  &first_page, 0, UI_STRING, PI_type_buffer,  0); /*错误*/
/*pid_item和pi_item都挂载到first_page下面，且pid_item的id（第四个参数）和pi_item的id相同，其结果未定义*/
/*不可在同一个页面下挂载id相同的obj*/
```

3. **无法进入一个页面**，有两种情况，第一种是这是一个空页面，第二种是菜单深度超出了栈的深度，只需要增加栈的深度（`fluent_ui_config.h`文件下的`MAX_STACK_LENGTH`宏）即可。
4. **不建议将page放在一个页面的第一项**，因为有时候你按键的消抖没做好或者按键方面的代码有bug，就会发生连续进入多个页面的情况。例如我设计了一个UI界面是`PID_page`，这个UI界面的第一项还是一个页面，当按键方面存在问题时会发生进入PID_page后马上又进入了下一个页面。



## 7.举个栗子 :chestnut:

将`fluent_ui`作为底层，新建`my_ui.c`和`my_ui.h`文件，并引用头文件: `#include "fluent_ui.h"`
### （1）设计界面：
**首先设计第一个界面，**

对于首页，我们使用FLOAT类型来展示速度、加速度、电流、电压，使用UINT16类型来展示已经行走的路程，使用INT8类型来展示某个程序变量TEMP，然后在首页挂载一个页面PID。
首先需要各自的变量和6个item和1个page。

```c
static int8_t   temp    = -50;
static uint16_t meter   = 9802;
static float    speed   = -3.4789;
static float    voltage = 6.9;
static float    current = 2.1;
static float    acce    = 0.841;
/*第一页中展示的内容*/
ui_obj_t speed_item, acce_item, meter_item, cur_item, vol_item, tmp_item;
ui_obj_t pid_page;
```
然后将他们都添加到首页(first page)中，first page在fluent_ui.c中已经定义好了。
```c
/*第一页中的内容（注意第三个参数均为&first_page）：*/
ui_add_item(&speed_item, "- Speed",   &first_page, 0, UI_FLOAT_ITEM,  &speed,   4);
ui_add_item(&acce_item,  "- Acce",    &first_page, 1, UI_FLOAT_ITEM,  &acce,    3);
ui_add_page(&pid_page,   "+ PID",     &first_page, 2);
ui_add_item(&meter_item, "- Meter",   &first_page, 3, UI_UINT16_ITEM, &meter,   0);
ui_add_item(&cur_item,   "- Current", &first_page, 4, UI_FLOAT_ITEM,  &current, 2);
ui_add_item(&vol_item,   "- Voltage", &first_page, 5, UI_FLOAT_ITEM,  &voltage, 1);
ui_add_item(&tmp_item,   "- Temp",    &first_page, 6, UI_INT8_ITEM,   &temp,    0);
```

**然后设计PID界面：**
对于PID界面，我们想要一个字符串类型的item用来显示我使用的是PID还是PD还是PI，然后需要三个浮点型的item用来展示Kp、Ki、Kd，最后需要一个check box用来控制电机的输出。
因此首先需要各自的变量和5个item：

```c
static float Kp = 1.2;
static float Ki = 0.5;
static float Kd = 0.7;
......
/*PID页面中展示的内容*/
static ui_obj_t PID_type_item, Kp_item, Ki_item, Kd_item, motor_item;
```
对于展示PID的字符串类型的item还需要一个用于挂载的字符数组：
```c
static char PID_type_buffer[10];
......
memset(PID_type_buffer, 0, sizeof(PID_type_buffer));
strcpy(PID_type_buffer, "*PID*");
```

将5个item添加到PID页面下：
```c
/*PID页面中的内容（注意第三个参数均为&pid_page）：*/
ui_add_item(&PID_type_item, "- Type",  &pid_page, 0, UI_STRING,         PID_type_buffer, 0);
ui_add_item(&Kp_item,       "- Kp",    &pid_page, 1, UI_FLOAT_ITEM,     &Kp,  1);
ui_add_item(&Ki_item,       "- Ki",    &pid_page, 2, UI_FLOAT_ITEM,     &Ki,  1);
ui_add_item(&Kd_item,       "- Kd",    &pid_page, 3, UI_FLOAT_ITEM,     &Kd,  1);
ui_add_item(&motor_item,    "- Motor", &pid_page, 4, UI_CHECK_BOX_ITEM, NULL, 0);
```

由于菜单最大深度为2，因此可以修改`fluent_ui_config.h`中的`MAX_STACK_LENGTH`宏至少为2。

至此，需要设计的界面都已设计完毕，最后调用`ui_settings_init`函数完成Fluent-UI底层的初始化即可。

### （2）界面互动：

使用四个按键，其原理图如下所示，连接按键的四个GPIO全部采用上拉输入。

<p align = "center">    
<img src="../PIC/key.png" style="zoom:30%;" >
</p>

具体的按键的驱动的实现在`multi_key.c`和`multi_key.h`中，按键的驱动较为简单且不是此文档的重点，因此只简单说明一下其原理：采用一个通用定时器进行1ms定时，不断检测和计算按键按下的时间，由此来判断按键抖动、按键短按、按键长按。

我们想要达到的效果如下表：

|      |      KEY1      |      KEY2      |             KEY3              |             KEY4              |
| :--: | :------------: | :------------: | :---------------------------: | :---------------------------: |
| 短按 |    光标下移    |    光标上移    | 对变量进行加法或点亮check box | 对变量进行减法或熄灭check box |
| 长按 | 进入下一个页面 | 回到上一个页面 |              无               |              无               |

其中KEY3在光标指向的为FLOAT类型时，将其加0.1，整形时加1，KEY4同理。

思路在第四章的后半部分完全类似，此处不再赘述，具体的代码如下（由于此处KEY3和KEY4完全类似，因此只给出KEY3的代码，完整代码参考例程即可）：

```c
void ui_add(void)
{
	 ui_type_t cursor_type = ui_get_cursor_obj_type();
	 switch (cursor_type){
		 case UI_FLOAT_ITEM:
			 ui_inc_cursor_obj_val(0.1);
			 break;
		 
		 case UI_UINT8_ITEM: case UI_UINT16_ITEM: case UI_INT8_ITEM:
		 case UI_INT16_ITEM: case UI_UINT32_ITEM: case UI_INT32_ITEM:
			 ui_inc_cursor_obj_val(1);
			 break;
		 
		 case UI_CHECK_BOX_ITEM:
			 ui_set_check_box_value(&motor_item, 1);
			 break;
		 
		 default:
			 break;
	 }
}
......
          if(key_state[KEY_3] == KEY_SHORT_PRESS){ /*KEY3被按下*/
			 ui_add();
		 }
		 
		 if(key_state[KEY_4] == KEY_SHORT_PRESS){ /*KEY4被按下*/
			 ui_sub();
		 }
......
```
## 8.功能测试

除了例程外，还提供了部分功能的测试代码，在TEST目录下，此代码只是测试Fluent-UI在各种情况下的工作情况，可不看。

