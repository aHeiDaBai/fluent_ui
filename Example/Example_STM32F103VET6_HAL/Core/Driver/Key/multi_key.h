#ifndef __MY_KEY_H
#define __MY_KEY_H

#include "main.h"

#define KEY_PORT_LIST                         {GPIOB, GPIOB, GPIOB, GPIOB}
#define KEY_PIN_LIST                          {KEY1_Pin, KEY2_Pin, KEY3_Pin, KEY4_Pin}

#define KEY_RELEASE_LEVEL           GPIO_PIN_SET             // 按键的默认状态 也就是按键释放状态的电平
#define KEY_MAX_SHOCK_PERIOD        (5       )                                 // 按键消抖检测时长 单位毫秒 低于这个时长的信号会被认为是杂波抖动
#define KEY_LONG_PRESS_PERIOD       (1000     )                                 // 最小长按时长 单位毫秒 高于这个时长的信号会被认为是长按动作

typedef enum{
    KEY_1,
    KEY_2,
	KEY_3,
	KEY_4,
    KEY_NUMBER,
}key_index_enum; 

typedef enum{
    KEY_RELEASE,          /*按键释放状态*/
    KEY_SHORT_PRESS,      /*按键短按状态*/
    KEY_LONG_PRESS,       /*按键长按状态*/
}key_state_enum;

void            key_scanner             (void);
key_state_enum  key_get_state           (key_index_enum key_n);

#endif /*__MY_KEY_H*/
