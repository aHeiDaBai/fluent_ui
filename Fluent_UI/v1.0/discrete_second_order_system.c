#include "discrete_second_order_system.h"

/************************************************************************************
* Function: [dso_system_init] 离散二阶系统的参数初始化
*
* Input: [sys]   创建的离散二阶系统结构体的地址
*        [T]     采样时间(即为屏幕的刷新周期，单位：秒)
*        [zeta]  对应到二阶连续系统中的阻尼比
*        [omega] 对应到二阶连续系统中的自然震荡频率
*
* Warnings: 此函数内部默认了初值(r(0)、r(1)、c(0)、c(1))均为0
*
* History:    2024-01-15 AHeiDaBai : First version
*==================================================================================*/
void dso_system_init(dso_system_t *sys, float T, float zeta, float omega)
{
	sys->b0 = omega * omega * T * T;
	sys->b1 = sys->b0 * 2;
	sys->b2 = sys->b0;
	sys->a0 = 4 + 4 * zeta * omega * T + sys->b0;
	sys->a1 = sys->b1 - 8;
	sys->a2 = sys->a0 - 8 * zeta * omega * T;
	
	sys->last_output = 0;
	sys->last_last_output  = 0;
	sys->last_input = 0;
	sys->last_last_input = 0;
}

/************************************************************************************
* Function: [dso_system_cal] 离散二阶系统的差分方程迭代法求解
*
* Input: [sys]   创建的离散二阶系统结构体的地址(必须经过了dso_system_init的初始化)
*        [input] 离散二阶系统这一时刻的输入
*
* Ouput: 离散二阶系统这一时刻的输出
*
* Warning: 1. 正常参数输入下（T > 0, zeta > 0, omega > 0），sys->a0不可能为0，
*             如果输入参数有问题，可能会导致sys->a0极小，可能导致output为Inf或者NaN
*          2. 此函数需要定期调用，调用周期为sys->T
*
* History:    2024-01-15 AHeiDaBai : First version
*==================================================================================*/
float dso_system_cal(dso_system_t *sys, float input)
{
    float output = sys->b0 * input + sys->b1 * sys->last_input + sys->b2 * sys->last_last_input - \
	               sys->a1 * sys->last_output - sys->a2 * sys->last_last_output;
	output = output / sys->a0;
	
	sys->last_last_input = sys->last_input;
	sys->last_input = input;
	sys->last_last_output = sys->last_output;
	sys->last_output = output;
	return output;
}
